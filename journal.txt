RandFill: Developer's Journal
================================================================================

2005-08-25: Project's Birthday
	- Erste Notizen, Gedanken zum Projekt.
	- First thoughts and notes regarding this project after discovering
	  "MP3blast" by Felix Opatz <felix@zotteljedi.de> sometime in 2005-06/07.
	  His program [description] inspired me for RandFill, yet funnily enough I couldn't even try it:
	  He took it down from his site around 2005-09, planning to do a rewrite, which didn't happen yet <http://zotteljedi.de/logbuch/2006-05-03.txt>.

2005-09-05
	Etwas in der Doku der Microsoft Platform SDK gewuehlt.
	Routine zum Feststellen von Hinzufuegen von best. Wechseldatentreger (z.B. USB-Stick) geschrieben.
	Berechnung der freien/belegten Kapazitaet.

2005-09-14
	Nach (erstweiliger) Fertigstellung eines anderen Projekts die Infos hierzu etwas geordnet.

2005-09-19
	Weitere Gedanken zum Design; einige Info zum Umgang mit Devices etc. unter Windows.
	Routine zum (rekursiven) Anzeigen von Dateien in Verzeichnissen gebastelt.

2005-09-20
	An der Playlist-Klasse gebaut; leider weniger geschafft als vorgenommen: Hab den groessten Teil des
	Tages mit den Feinheiten von Dynamischen Arrays (= Zeiger-auf-Zeiger) unter C (xalloc()) gekaempft.

2005-09-21
	Playlist: Interne Datenverwaltung veraendert; benutzt nun eine struct.
	Transport-Klasse begonnen, erste Kopiermethode entwickelt/getestet.
	CpyCtor u. AssignmOp erstellt.
	Probleme dabei mit Speicherzugriffverletzungen, die nur unter Visual-C++ auftauchen. Hmm...

2005-09-22
	Argh! Und wieder einmal war eine Uninitialisierung das Problem (Speicherzugriffverletzungen unter Visual-C++-Kompilat):
	
	Beim CopyCtor habe ich den bestehenden Zeiger per realloc angelegt (Zur Erinnerung: Falls es ein Null-Zeiger ist, wird der
	Speicherblock neu angelegt statt erweitert, d.h. realloc() arbeitet wie malloc()). Unter gcc war der Zeiger
	ansch. auf Null gesetzt, unter VC nicht.
	
	Aendern auf "malloc()" bzw "Erst Zeiger nullen, dann realloc()" half.
	
	Eigentlich haette doch auch realloc funktionieren sollen... Im zuvor in main() angewendeten AssignOp wurde der Zeiger
	zwar vorher geloescht, aber danach ja auch wieder angelegt -- allerdings neu (malloc()).
	Also wurde beim CpyCtor danach  zunaechst faelschlicherweise dieser Zeiger erweitert (realloc()) -- ich brauchte aber
	einen neuen, da eine Kopie erzeugt wird. Ja, das koennte die Begruendung sein.
	
	Weitere Erkenntnis/Problemursache: "strncpy() does not '\0'-terminate the string if it's third argument is <= the length of the second argument."

2005-09-27
	Dazugekommen: Methode zur Erzeugung von (Pseudo)Zufallszahlen aus einem Bereich von [0, n).
	              Einbindung des Zufallselements in die Kopierroutine.
	
	Je nachdem wie gut oder schlecht sich diese Routine erweist (die uebrigens nicht bon mir stammt), muss ich
	in Zukunft nochmal da ran gehen.
	
	Auf jeden Fall scheint das Problem der Zufallszahlen nicht ganz trivial zu sein (wenn man sich nur "nebenher"
	dafuer interessiert); zumindest haben die Archive der einschlaegigen C++-Foren, Sites und Buecher
	keine "ultimative" Antwort liefern koennen.

2005-09-2x
	- Added routine to detect removable mass-storage devices.
	- Added routine to calculate free/used capacity on such devices.
	- Added routine to display/list files in a directory.
	- Added method for generating (pseudo) random numbers in the range of [0, n). Integrated this into the copying-routine.
	- Developed/tested copying-routine (class "Transport").
	- Working on the "Playlist" class (and struggeling with dynamic array).
	- Fixed a bug of the kind "uninitialized data": Memory access violation when built with VC++ (caused by different behavior of realloc()/malloc() und gcc/VC and null/non-null pointer).
	- Fixed a bug since strncpy() "does not '\0'-terminate the string if its third argument is <= the length of the second argument."

2005-10-12
	Lange Zeit her, seit dem letzten Eintrag. War aber nicht untaetig (im Gegenteil), aber hatte dann doch keine
	Lust, nach jeder winzigen Aenderung einen Eintrag zu schreiben.
	
	Inzwischen ist das Programm ziemlich gediehen, wuerde mal sagen "Version 0.9"---mir fehlen noch ein, zwei Features,
	die ich auf meinem Konzeptpapier stehen habe, aber dann erfuellt es soweit seinen Zweck.
	
	GUIs mittel purer Win32-API zu basteln ist echt ein Krampf, auf MFC oder eine 3rd-Party-Lib wie Qt oder wxWidgest
	wollte ich (noch) nicht setzten. Leider unterstuetzt die Beta 2 von VC8Express auch Resourcen z.Z. nur mangelhaft.
	
	A propos: Die Express-Beta hat irgendeine Macke in ihren Headers/Lib und kurz vor Toreschluss (Impl. der INI-Funktionalitate)
	musste ich wieder auf das Toolkit und die Kommandozeile/Makefile ausweichen (wenn man eine IDE erstmal gewohnt ist
	wieder etwas gewoehnungsbeduerftig). Hab bei der Gelegenheit die Makefile aktualisiert und gesplittet (VCTK und MinGW).
	
	Leider baut MinGW das Projekt noch nicht ganz, verschluckt sich an u.a. einigem Windows-spezifischen Code...
	Naja, mal schauen.
	
	Ausserdem habe ich eine Feature-Comparison von VC8 Express, Standard, Team, usw.
	heute gesehen: Der Express-Edition gehen ja doch 'ne ziemlich Menge ab und die Std-Variante schlaegt mit 299 US-Dollar
	zu buche... :-(
	
	Das waer's erstmal.

2005-10-17
	Filterung eingebaut (simples Wortabgleich auf Dateiname und Pfad, accept/deny), allerdings fehlt dazu noch
	(wie zu einigen anderen Featurs) eine passende GUI; plus weitere kleinere Verbesserungen/Aenderungen.
	
	Bin momentan noch am schwanken, ob ich komplett eine andere Oberflaeche nehmen soll (beispw. Qt, wxWidgets),
	oder ob das nicht doch Overkill ist.

2005-10-18
	Heute u.a. Statuszeile, Fortschrittsbalken (in der Statuszeile!) eingebaut und Layoutaenderungen vorgenommen.
	
	Das 1.0.0-Release rueckt immer naeher: Alle Features fuer diese Version sind grundsaetzlich implementiert,
	mir fehlen noch zwei GUI-Frontends (zur Bedienung der Filter und mehrerer Quelldatenverzeichnis) plus
	ein seperater Thread fuer den Kopiervorgang, damit die Anwendung bedienbar bleibt bzw. der Vorgang angezeigt wird.
	
	GUI-Apps mit der reinen Win32-API und ohne IDE zu erstellen ist nicht gerade toll---und dies hier ist ja
	nur ein geradezu winziges Tool und ich wollte es ja so (kein MFC, ATL/WTL, .NET. etc; das mit dem IDE-Verzicht
	hat sich eher so nebenbei ergeben).
	
	Davon abgesehen mag ich diese Verzahnung und Verzettelung zw. den unterschiedlichen
	Komponenten nicht: Aus fast jeder Datei und Klasse werden inzw. Win32-API-spezifische Messages versendet oder
	Routinen aufgerufen :(

2005-10-19
	Falls es wider Erwarten Probleme macht:
	- Playlist::get(Directory|Filename)(int i) hinzugefuegt.
	- Filter ist kein friend von Playlist mehr.
	
	Deweiteren:
	- remove(File|Directory) hinzugefuegt.

2005-10-xx
	- Began work on new feature: Filtering, based on simple comparion between key-phrase and file/path, accept/deny-action).
	- GUI now exists (written in plain Win32-C-API).
	- Added statusbar, progressbar (within the statusbar), ...
	- Added Playlist::getDirectory(), Playlist::getFilename(), Playlist::removeFile(), Playlist::removeDirectory().
	- Still lacking: GUIs for the filtering mechanism and for selecting multiple source directories.
	- Still lacking: Worker thread for copying (otherwise the GUI doesn't respond).
	- Split the Makefiles into two, one for VCTk and one for MinGW.
	- MinGW still lacks something/chokes on Win32-code and thus doesn't yet build the application.

2005-11-01
	Weiteres Vorantschreiten; Filterung scheint endlich zu funktionieren (zumin. nach meinen Tests).
	Fehlen noch ein paar Feinheiten (Save state/load settings etc.) und ggf. noch ein Umbauen der GUI.
	(Plus einiger "Aufraeumarbeiten" am Quelltext).

2005-11-05
	Bug/fixes:
	
	Die Zufallszahlen, auf denen die Auswahl der Tracks beruht, wiederholten sich.
	Nach einigen Tests habe ich festgestellt, dass es an srand() lag, besser: An dessen Stelle im Code.
	Hab srand() aus main()/WinMain() in Playlist::copy() gepackt und gut war.
	
	Offene Frage: Ist mir das vorher nicht aufgefallen, oder ist dieser Fehler erst dadurch entstanden,
	dass der Kopiervorgang inzw. in einem eigenen Thread arbeitet und die von srand() erzeugte Daten
	(welche?) thread-lokal sind?
	
	UPDATE 2005-11-10:
		Yup, bin nicht der einzige, dem sowas passiert ist:
		[18:00:39] <dr_evil> mmu_man does srand() and rand() use TLS in BeOS?
		[18:00:59] <dr_evil> I know that in Windows it seems to use per-thread data
		[18:01:22] <dr_evil> because calling srand in WinMain has no effect on rand() in threads
		[18:02:37] <dr_evil> although it'S ligical, I really didn't like it
	
	Allerdings produziert die pure generateRandomNumber() in meinen Utilities auch ganz ohne srand() regelmaessig
	unterschiedliche Zahlen...
	
	Ausserdem noch an einer Stelle in PL::copy den Counter erhoeht, da sonst der Loop ca. 10x oefter ausgefuehrt wurde
	als Dateien da waren.

2005-11/12
	- Filtering works now.
	- Fixed: Counter in Playlist::copy() grow larger than there were entries on the playlist.
	- Fixed: The "random number" was too repeatitive.
	  Reason was srand(), or rather its placement in the code.
	  Moved it from WinMain() to Playlist::copy().
	  Notes: TLS, Windows uses per-thread data; calling
	  srand() in WinMain has no effect on rand() in threads, but the plain
	  generateRandomNumber() did gen. rand. nr., even without srand()...

2006-01
	- Progress of copying is now displayed in a dialog.
	- Work on GUI (handmade .rc files, since VC++Express doesn't come with a dialog editor/found no good free one yet).

2006-02/03
	- Option "clear device": The default (if enabled) is now to delete all types files.
	- Fixed bug where the "clear device"-option didn't work correctly when invoked a second, third, n-th time after being stopped. Caused by invoking TerminateThread(), which is normally a no-no since it doesn't free all resources. Changed the code to use CreateEvent/WaitForSingleObject/SetEvent to cancel the worker-thread.
	- Work on the handling of the application-wide settings.
	- New module: Created a structure for the settings; moved loadSettings() and saveSettings() procedures to separate file.
	- New feature: An user can now limit the amount of available space that the copy-routine will use. The GUI front-end for this is a trackbar (slider), with which the percentage will be set.
	- Switched to scons build system (only for the MinGW build at the moment).
	- Beginning to use Subversion (Revison Control System).

2006-03-28: Version 1.0.0
	- Not a public release.
	- GUI rework: Now using a tab-view/control.
	- Communication works now via sending custom messages instead of directly manipulating controls.
	- Reordering of Playlist::copy(): No more blank status while the first file is being copied.
	- Split the GUI from main.cpp into several smaller win32gui_*.cpp files.
	- Added radio-buttons for "empty device", so that the user can better choose between the options.
	- The program is now capable of ejecting a [removable mass storage] device when the copy-job is done.

2006-05-08: Version 1.0.1
	- First public release, i.e. put on my homepage.
	- Further work on seperating GUI from logic.
	- Added Unique IDs to each device again.
	- Added translation strings.
	- Changed in [util.cpp]: Now always using C++'s min/max insteand of Microsoft's macros.
	- Fixed: Threading issues that caused crashes.
	- Integrated status information from separate dialog window back into the main window.
	- Moved actions to save settings and to eject a device from copy/thread routine to handler of 'done'-message.
	- Removed Playlist::initiateListRebuild() and MSG_SOURCE_UPDATE_LIST, replaced its functionality by adding Playlist::getDirectories() and [win32gui_source.cpp] refreshListbox() (portable and win32-specific parts).
	- Fixed: Copying stalled with a high CPU load at some point after cancel/restarting a job, but copying finished and the device got ejected.
	  GUI/Program didn't block, since cancel would exit the application.
	  I'm still not sure where/why this happend (the byte_limit was recalculated after canceling, etc.), but two things seem (!) to have this solved:
	  (1) Giving WaitForSingleObject(copythread, ...) a higher time-out than 0ms.
	  (2) Not closing the thread/event handles (too soon).
	- Improved feedback for the user:
	  o When copying has been finished/aborted.
	  o The title bar changes text when minimized to taskbar and a copy job is running (displays percentage).
	  o Window/Taskbar symbol flash when done with copying.
	- Window now starts in the center of the screen (adapted code from the MS Platform SDK).
	- Removed the separate "settings" structure (using now class members and globals).
	- Created a makefile for MinGW again.
	- Improved keyboard navigation.
	- Added Dialog styles for better keyboard navigation. Work in progress.
	- Added DeviceManager::empty(int index); forwarding to method of class "Device".

2006-07-15
	The .exe depends on two of my own libraries:
	- initfile.lib
	- win32aux.lib (new today)
	
2006-07-17: Version 1.0.2
	- User can now change GUI language (german/english; requires application
	  restart); settings are being saved in file.
	- Added menubar for switching languages and invoking an info box.

2006-08-21: Version 1.0.4
	- Graphical user interface (GUI) re-arranged
	- no more tab controls; a separate "Filter" dialog, invoked via menubar.
	- Added feature to open target device in file explorer.
	- List boxes (Source and Progress) scroll now also horizontally.
	- Source's path display is now also change button (removed separate button).
	- Fixed regression, where starting point for browse-for-folder dialog wasn't accepted.
	- Various internal or minor changes.
	- (No version 1.0.3 released)

2006-09-25: Version 1.0.5
	- The UI language can now be changed while the program is running.
	- Progress-Listbox now scrolls the name of the latest copied file into view automatically.
	- The button to select the source folders now displays either the last path
	  (from settings.ini) or a hint ("Please select...").
	- The button to select the source folders now displays a shortened text
	  if the path is too long.
	- Rearranged position/size of some GUI controls, changed strings.
	- Implemented/added calls to lockControls() to prevent unexpected user-input via the GUI.
	- Put some global variables into local scopes; got rid of some unused/forgotten variables.
	- Fixed the short window-flashing at start-up.

2006-10-08: Version 1.0.6
	- The list-controls (source, progress) show horizontal scrollbars only when neccessary, and also adjust them to in the correct width.
	- Progressbar has changed style: From 'smooth' to the default segmented bar.
	- Statusbar is cleared before a new copy-job begins.
	- Progress status report when minimized: (1) Loads string from resource; (2) flashes only once when done.
	- Moved progressbar into the right side of the statusbar.
	- List (of copied files) and progressbar only visible when job is running/done.
	- Satellite Resource DLLs now only contain stringtable; moved dialog scripts to a central .rc file in the main project.
	- The menu is now build via code, not RC-scripted (due to string-loading issues when switching languages).
	- New 'About..' window: Dialog instead of message box; with clickable text for web- and e-mail address (source code by Neal Stublen).
	- Bugfix: Canceling a copyjob while the option 'eject device' was checked could cause the program (and even Windows' Explorer) to hang.

--------------------------------------------------------------------------------

2007-04-29

After a rather long hiatus, it looks like I'll be back -- soon.

[some text deleted to protect the innocent, 2013-11-30]

Over the previous months I've collected plenty ideas for new features and discovered bugs that need to be fixed.
But before any bigger changes, I need to redesign major parts of the program: Several of the improvements will require a more
interactive and better GUI -- something that I don't want to handle in plain Win32.

RandFill at its current, albeit small, state has reached the critical point for me where it isn't feasible to add more stuff
as long as it is based on pure Win32-API calls for GUI/OS tasks; it just gets too messy for my taste.

So, again, I "researched" the (un)usual supects regarding toolkits/frameworks to ease a programmer's life.
Discarding the not so poplular libraries, I was (and still am) torn between MFC, WTL, wxWidgets, QT and even .NET.

Platform-portability in general is desireable, but realisticly, not a important goal for my GUI-app at the moment:
I don't use MacOS or Linux; Mac users are a rather picky bunch and neither Qt nor wxWidgets are highly regarded in those circles
for consumer applications. Linux on the desktop isn't an easy target (x distros, y versions, z subsystems) and not used by me.
What remain are niches and the 90+% of Windows PCs.

.NET is too much baggage for my app.
Qt has a very nice C++ API, but the non-commercial edition can't be used with Visual C++.
I tested shortly wxWidgets, but it somehow felt still (after 12 years) unfinished, unpolished, unstable, with old-fashioned idioms, etc.
WTL is the little brother of MFC; unknown, not so comprehensive, but supposedly a bit more progressive.
MFC ist the well-know, tested all-rounder with 15-year old warts and issues.

(Since Visual C++ 2005 Express Edition could be patched to use WTL, but doesn't come with resource editor or
support for MFC, I had to install Visual C++ 2003 .NET (Academic)).

After all that, I still haven't totally made up my mind, but the current ranking list looks like this:
1. MFC
2. WTL
3. wxWidgets
4. Qt
5. .net

I did text wxWidgets months before, looked at WTL before and downloaded an eBook for MFC.
The day after tomorrow is a holiday, so I will try to finally decide which route to take and then begin to work on RandFill 1.1/2.0.

--------------------------------------------------------------------------------

2007-05-20

After some struggling with "Microsoft Visual C++ .NET 2003", I do now have
build/project files for this compiler added to the repository.
Main reason is the resource editor and support for MFC; both aren't supported on
VC2005 Express, and my copy of 2003 is a full version (academy license).
But actually I've yet to come to a final conclusion, whether I should stick win
plain SDK/Win32-API, or switch to MFC or WTL (other options are currently out).
By the way: I needed to rebuild InitFile.lib and win32aux.lib for "2003", but
did so on-the-fly, meaning I did not check-in the edited files for 2003!

Later, the same day...

Added, edited, deleted files... the resources are now created by the VC tools,
not by hand anylonger (so don't bother editing resource.h).
Multi-language support is gone for now! Will probably add it again some time in
the future (after the switch to MFC).
Had to disable a couple of features to make it compile today (in Info dialog,
for example).

--------------------------------------------------------------------------------

2007-05-22

Removed some code, mostly code to manually check and handle (i.e. en-/disable)
controls (radiobutton, checkbox, etc.).

The default behaviour is now being controlled by the settings for the control;
although it simplifies things a bit, I'm not quite sure: The increased
dependency on GUI tools (do not touch resources, VC overwrites it each time!)
in this context is new to me.

OTOH, to layout the controls via grid and/or guidelines is way more easier than
editing RC files, compile, run... edit again...

--------------------------------------------------------------------------------

2007-06-09

After investigating the GUI options, I've decided to use the Windows Template
Library (WTL) for now/for this rewrite. So far, it's OK.

Spent the last few evenings/weekends mostly with adapting the code to WTL.

Now the program is almost at the state of function it was before the conversion
and soon I hope to invest time again in adding features, improvements and
bug-fixing instead of reinventing the wheel.
                                                                       
--------------------------------------------------------------------------------

2007-07-15

Versionsinfo ausgelagert; und noch ein passendes Python-Skript gebastelt, das
am Ende eines Build-Vorganges die div. Werte automatisch aktualisiert.
Hat mich fast den ganzen Nachmittag gekostet und in Visual C++ ist es noch
nicht eingebunden... Nachtrag: N�, VC++ macht erstmal nicht mit :-/

ChangeLog.txt wiedereinfeguehrt. Das Journal wird ausfuehrlicher bzw.
detaillierter ueber Aenderungen, Entscheidungen/Entscheidungsprozesse usw.
berichten, auf deutsch oder englisch; waehrend das Changelog eher stichwortartig
die Highlights fuer andere Anwender zusammenassen soll.

--------------------------------------------------------------------------------

2007-07-16

Habe also mit einigem Googlen doch noch WTL unter Visual C++8 Express wieder zum
Laufen bekommen (wobei mit anderen Kniffen als ich sie in Erinnerung hatte).
Ausserdem ResEd als Resource Editor eingespannt.
Alles in allem baut/laeuft es erstmal wieder.

--------------------------------------------------------------------------------

2007-07-24

Da Vista ja eine haertere Gangart einlegen will, was Sicherheit betrifft (hmm...),
wurde es Zeit, die settings.ini aus dem Programmverzeichnis zu holen und in
die 'Anwendungsdaten'-Verzeichnisstruktur des jeweiligen Benutzers einzubinden.

Hat mich einiges an Zeit gekostet, weil die Datei erst gar nicht geoeffnet bzw.
geschrieben wurde, dann nur unterhalb von 'All Users', dann nur geoeffnet, aber
nicht geschrieben...

Letztendlich lag es daran, dass CreateFile() mit dem jeweiligen korrekten
dritten Parameter ausgestattet wurde: Statt 0 mit FILE_SHARE_READ (bei
GENERIC_READ) bzw. FILE_SHARE_WRITE (bei GENERIC_WRITE).

--------------------------------------------------------------------------------

2008-09-08

InitFile (Lib), win32aux (Lib) und RandFill (App)
k�nnen seit heute alle unter Microsoft Visual C++ 2008 Express Edition gebaut
werden (seit dem Neuaufsetzen des PCs vor ein paar Woche z.Z. meine einzige
IDE).

--------------------------------------------------------------------------------

2010-02-01

Neuer Rekord: �ber ein Jahr ohne Update... egal, Zeit wieder in Gang zu kommen.
TODO.txt mit den Papiernotizen der letzten Jahre aktualisiert und etwas umorganisiert.
Ein richtiger Issue Tracker w�re nicht schlecht, aber vmtl. auch Overkill.

N�chster Halt: USB-Handling in eine DLL auslagern.

--------------------------------------------------------------------------------

2013-07-21

Drei Jahre sp�ter(!) und es geht weiter.
Inzwischen ist Windows 7 64-bit und Visual Studio 2010 der Stand der Dinge
(wobei Windows 8.1 und VS 2013 bereits angek�ndigt sind).

Hab heute RandFill, InitFile und Win32Aux auf den Stand gebracht, da� sie in
dieser Umgebung gebaut werden k�nnen; nicht sonderlich aufwendig: Paar Pfade und
Einstellungen anpassen hat gen�gt.

--------------------------------------------------------------------------------

2013-10-20

Hab mich in den letzten Tagen nochmal in die Thematik "(Windows) Installer"
eingelesen und mich dann f�r NSIS (Nullsoft Scriptable Install System) entschieden
und dort eingearbeitet. Es ist skript-basierend, leicht erlernbar und f�r die
aktuellen Anforderungen ausreichend (Datei kopieren, Links anlegen, usw.).

Zun�chst war es etwas abschreckend, dass das Projekt seit 2009 offenbar ohne
Fortschritt vor sich hind�mpelt, aber es gibt aktuell Wiederbelebungsbem�hungen
(inklusive eine 3.0-Alpha), aber auch die alte Version funktioniert noch ohne
Probleme unter Windows 7 64-bit.

Hab nach einigen Tests dann also heute RandFill-1.1.0-Setup.exe ver�ffentlicht.

--------------------------------------------------------------------------------

2013-12-05

The project is now a public (Mercurial) repository on BitBucket.org
and published under the terms of the "MIT license".

--------------------------------------------------------------------------------

2014-04-27

Switched to the CMake build system instead of using Microsoft Visual Studio
solution files directly; i.e. use CMake to generate the MSVC files, then
work/build within MSVC as usual (currently only MSVC is supported/tested).

--------------------------------------------------------------------------------

2014-06-29

Began to switch from plain Win32 to Qt -- work in progress.
Worked on the build workflow (CMake) and installer (NSIS).

--------------------------------------------------------------------------------

2014-12-06

Status update: The CMake-based build process works well now, the same goes for
the Nullsoft-based installer. The Qt-port is also progressing (slowly); the
missing part, until it's feature-even with the current version of the program,
is the handling of the USB devices.

--------------------------------------------------------------------------------

2015-02-04

Handling of USB mass storage devices works again in the Qt port.
Using Unicode (mostly) now.
The multithreading is Qt-code, the file copying is still based on Win32-code.
'Eject device' doesn't work yet -- but didn't work with release 1.1.0 under Windows 7 (64-bit) either.
Some features need testing; but in general, the feature complete transition is almost done.

--------------------------------------------------------------------------------

2015-02-07

Revised Issues.txt: Moved still relevant parts to the Issue Tracker of the BitBucket.org page
(and made an backup/export of the data, just in case); issues.txt removed.
The plans/ideas moved to Roadmap.txt.

--------------------------------------------------------------------------------

2015-02-08

As it turned out, the reason why "Eject device" (USB storage) didn't work under
Windows 7 was a change under the hood, starting with Windows Vista.

Really helpful was this article:
http://www.codeproject.com/Articles/13839/How-to-Prepare-a-USB-Drive-for-Safe-Removal
Changed the code, works now again under Windows XP (32-bit) and Windows 7 (64-bit).

At first, I thought the Win7-fix had introduced a regression in the WinXP code;
but it was due to a lack of rights of the user on my testing machine (manually
ejecting the storage device wasn't allowed either, running the program with
an administrator account did work).

--------------------------------------------------------------------------------

2015-03-07

Hg Changeset ID 763f128201fccf3ee67063f51ae2f24362a8dfe5 -- Based on the commit
message, this wasn't any big work, obviously. But the path that lead to this
commit took approximately a week of late night sessions after work and was paved
with cursing, whining and times where I wanted to tear my hair out.

It started when I a single line of code, that invoked a simple toStdWString
method on a QString object, so nothing fancy.

But at that point, the program crashed, when the QString exceeded a certain
length (at first).

I looked over the code path again and again, but I couldn't make neither head
nor tail of it.

In the end, it was, as so often and as I already suspected, a matter of rather
trivial modifications to fix it. Until that, it was a bumpy road.

Many attempts with printf and so on resulted in nothing, and when I began
building the program in debug mode, things got even worse: crashing earlier and
also on other, but similar sections.

So after a lot of trying, reading and searching on the web, my approaches of
solving this got more and more complex. And I thought to myself that this
shouldn't be necessary, those solutions are overkill for such a simple statement
of code.

Still, the errors and crashes didn't disappear; error messages of "access
violations" appeared, even though my code never directly or explicitly(!)
handled any memory at that point.

And then, suddenly, after another round of extensive google'ing with another set
of keywords, I stumbled over two or three posts that showed me the light:
- "This is a known symptom if you're mixing debug and release DLLs."
- "If after this modification you are still getting a bunch of linker errors in
  your project (probably because you are still linking to binary libraries which
  are compiled with a different flag), [...]"

Yes! Of course! I checked my projekt settings, changed them -- and it worked!

I had built my application with static linking of the runtime library (/MT),
while the DLLs of my build of Qt used dynamic linking (/MD). And for whatever
reason, this incompatibility only now had a bad effect on my program.
So I switched to /MD and the crashes in my release build vanished.

Also, the reason why the the debug build was even more fragile: I only built Qt
in release mode for my own use. The debug build still isn't 100% error free,
but later on, I might also build Qt in debug mode and certainly (hopefully) this
will get rid of the remaining defects.

So simple, if one only knows where to look.

--------------------------------------------------------------------------------

2015-05-07

This will be the last/final entry in the journal!

Not because I've stopped working on RandFill, quite the contrary: Although I'm 
busy writing code, I sadly never think about updating the journal.

I've started this before I began using a version control system, before I had a
blog and before I put the source code on a hosting site -- nowadays, all these
means fit better in my workflow to document my work than opening this file
in an editor and to repeat what I just wrote in a commit message or blog post.

As a kind of replacement, I'll write more into the History.txt from now on
(a file that needs to be updated frequently, anyway).

That's because I've stumbled upon such files at other projects recently, and
liked what I read there: A middleground between release notes and changelogs.

Previously, my ReleaseNotes.txt was meant as a high-level overview for normal
users, to inform them about notable, majore changes. But that also meant skipping
lots of more technical information. On the other hand, a full blown changelog,
automatically generated from all the commit messages, is too noisy most of the
time; and to filter that out again by hand is too much work.

The journal.txt will be removed from the repository after this commit;
I'll keep a backup locally.

--------------------------------------------------------------------------------

2016-11-21

... and back from the dead!

I somehow still need a project-local outlet for my thoughts on development,
besides succinct commit messages and entries in the History/ChangeLog file.

So, what have I've been up to, RandFill-related, in the meantime?
Released two more versions (1.2.0 and 1.2.1) and brought a sister project up to
a version 0.1.0: The WPD library (WPDlib), for handling MTP storage devices like
smartphones.

This library has yet to be integrated into RandFill; after that, a new mechanism
of filtering is also high on the list (probably based on a SQLite database
backend), next to all the other unfinished stuff on the roadmap.
By the way: Sooner or later the text of the Roadmap.txt will likely be merged
into this file.

But the immediate changes, to get slowly back in the groove, is to update the
two supporting tools: Qt and NSIS.

Qt has released a version 5.6.1, which isn't the newest version (that would
currently be 5.7.0), but it is a Long Term Supported (LTS) release, with support
for the next three years (so approximately until March of 2019).
And since my little program here doesn't live on the cutting edge, I'm very fine
with using such a version. Another aspect is that from 5.7.x on, support for
Visual Studio 2010 (and other older compilers) has been dropped; and I'm not yet
ready to switch -- although I do have my eyes on Visual Studio 2017, which is
currently in testing.

And Nullsoft Scriptable Install System (NSIS), "the script-driven installer
authoring tool for Microsoft Windows", has finally published version its 3.0.
No earth-shattering new features, as far as I'm concerned, but support for
Unicode and Windows 10 is welcome.

Speaking of which: Windows 10 support is another topic on the agenda, but no
so urgently. I've tested the current version of RandFill under Windows 10
already (while the OS was a beta/release candidate), and it seemed to work fine.
My main work/developement computer will probably stay with Windows 7 for the
foreseeable future, but I might replace my notebook soon (which currently runs
32-bit Windows Vista -- hey, it's not that bad for normal tasks and it's a
good testing ground...).

--------------------------------------------------------------------------------

2016-11-23

Updated my own build of the Qt framework from 5.2.1 to 5.6.2 (and also wrote a
new post about it for my blog, as before; mainly as a reminder for myself).
Adjusting RandFill was luckily an easy exercise: Just had to change the base
path and one specific folder in two CMakeLists.txt.

Also upgraded the Nullsoft Scriptable Install System (NSIS) on my system from
2.46 to this summer's current version 3.0. That was even easier: Nothing had to
be changed.

--------------------------------------------------------------------------------

2016-11-24

Upgraded CMake on my system from 2.8.11 (32-bit) to 3.7 (32-bit).
Recreating the Visual Studio 10 solution (64-bit) and building the whole project
without any problems after that.

--------------------------------------------------------------------------------

2017-08-26

Getting back in the saddle after a long time (was a bit burned out, also moved to a new place,
plus some personal matters...).

Still using Windows 7 (will move to Windows 10 when I build a new PC, maybe this fall...), but
upgraded to Visual Studio 2017, Windows SDK 10, and upgraded to Qt 5.9.1, CMake 3.9.1, NSIS 3.02.1
and Mercurial (Hg) 4.3.1.

After some adjustments (mostly getting CMake to find the Windows 10 SDK), "RandFill" now builds
with VS15 and Qt 5.9.1. But after I close it, Windows throws a 'doesn't work anymore' error at
me; strange, need to investigate later.

___________
END OF FILE
