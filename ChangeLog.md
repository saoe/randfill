﻿[RandFill](http://www.saoe.net/software/randfill) Change Log
====================================================================================================

Notable changes to this project. Inspired by [Keep a Changelog](https://keepachangelog.com/).


a.b.c (20xx-xx-xx)
------------------
* [CHG] Tools updates, now built with:
    - Visual Studio 2019 Community Edition
    - Qt 5.12.3
    - Nullsoft Scriptable Install System (NSIS) 3.03
    - CMake 3.15.3


1.2.1 (2015-07-16)
------------------
* [ADD] Directory on the current target device is being watched for file-system changes (at the moment
        only size) to trigger a recalculation of the free capacity; the obversation happens in a worker-thread.
* [ADD] The progress dialog now displays the amount of files copied so far.
* [FIX] The button to remove a source path is only enabled if at least one item (path) in the source list is selected.
* [ADD] The Close button on the copy-progress dialog is disabled; handling of the Escape key is added.
* [ADD] The file operation (copying) checks for success/failure.
* [ADD] The program differentiates between the copying being aborted by the user or by the system (due to an error in the file operation).
* [CHG] Other minor changes.


1.2.0 (2015-05-17)
------------------
* [INF] Published under the terms of the MIT license and code is available via a public Mercurial repository on BitBucket.org.
* [CHG] Improved installer (Nullsoft Scriptable Install System/NSIS):
    - multilingual: currently support for English and German.
    - installs required Microsoft Visual C++ Redistributable Package.
* [ADD] Capability to create a playlist file (in M3U or PLS format).
* [ADD] Program's multilingual (englisch and german, for now); can be switched on the fly; see menubar.
* [ADD] Comes with a (HTML) help file; see menubar.
* [CHG] Reworked much of the code. Switched from plain Win32-API to the Qt framework for the GUI and several other components.  
  It's nevertheless far from being a multiplatform program: The code that handles the devices is still mostly Win32-based.
* [ADD] Uses "CMake" as a meta-build system; support for 32-bit and 64-bit builds.
* [ADD] Misc. GUI stuff:
    - Menu bar with entries for language selection, invoking help and displaying information about the program.
    - Spin-box widget for controlling the free capacity that should be used on the target device.
    - Explorer windows can be opened for a selected item on the sources list and for the target device.
    - Tooltips with explanatory text when hovering over some elements.
    - A message box shows up if no files are found in the source directory that you want to add.
    - Vectorized the logo (to get the icon in a higher resolution).
    - Button "Filter..." is gone for now, since neither the old nor the new functionality are working/existing at moment.
* [FIX] Bugfixes:
    - Issue #2: Ejecting an USB storage device working again under Windows 7; old method didn't work due to changes in Windows Vista.
    - Issue #3: Selection of 'source' was defined as 'empty' when the the uppermost entry is removed.
    - Issue #4: Bug when middle entries of 'source' (directories) list are removed.
    - Issue #7: Empty device before copying only deleted files, but not folders/directories.
    - Issue #9: Add path button was inactive after selecting source directory.
    - Files would be copied into the target directory on the device, even when the user had unchecked the option.


1.1.0 (2013-09-01)
------------------
* [INF] Update 2013-10-20: Added a NSIS-based installer and re-released the
        program with the same version number.
* [ADD] Latest code changes were from 2007-08-04/06, left unpublished for years.
* [CHG] Project builds now with Visual Studio 2010 and runs on Windows 7.
* [ADD] Added option to put the files in a sub-directory on the target device.  
        This should fix the 'capacity bug' (Issue #6), which is probably related to a FAT16 limitation.  
        Note: If the device should be emptied prior to copying, and the option for a target directory is
        set, only that directory's content is affected.  
        Also (reminder), only files get deleted, no directories.
* [CHG] GUI: Swapped order of 'source' and 'target' sections.


1.0.7 (2007-07-28)
------------------
* [CHG] Now using "Windows Template Library (WTL)" for the GUI instead of pure Win32-API calls.
* [REM] Menubar gone, multi-language support gone (for now...).
* [CHG] Internal changes, GUI tweaks. No new features; only minor improvements and bugfixes.
* [CHG] Configuration file is now located below the user's application data directory (i.e. settings
        are per user), instead of the program directory.


1.0.6 (2006-10-08)
------------------
* [ADD] New 'About' window.
* [CHG] Hide list of copied files and progressbar when no copy-job is running.
* [FIX] The list-controls (source, progress) show horizontal scrollbars only when neccessary,
        and also adjust them to in the correct width.
* [CHG] Moved progressbar into the statusbar.
* [CHG] Progress status [when minimized] flashes only once when done.
* [FIX] Statusbar is cleared before a new copy-job begins.
* [FIX] Bugfix: Canceling a copyjob while the option 'eject device' was checked could cause the
        program (and even Windows' Explorer) to hang.

      
1.0.5 (2006-09-25)
------------------
* [ADD] The UI language can now be changed while the program is running.
* [FIX] Progress view: List now automatically scrolls to the latest entry.
* [CHG] The button to select the source folders now displays either the last path (shortened if too long) or a hint.
* [CHG] Rearranged position/size of some GUI controls, modiefied labels.
* [CHG] Most GUI elements are now locked while copy-job is running.
* [FIX] Fixed the short window-flashing at start-up.


1.0.4 (2006-08-21)
------------------
* [ADD] Added ability to open target device in file explorer.
* [CHG] Rearranged and tweaked GUI: no tabbed view any longer, the filter button has been moved to the menubar, etc.


1.0.2 (2006-07-17)
------------------
* [ADD] GUI language can be changed (to german and english), requires the application to be restarted;
  the state is saved in the settings file.
* [ADD] Added menubar for switching languages and invoking 'about' info.


1.0.1 (2006-05-08)
------------------
* [INF] First public release, i.e. put on my (Arcor.de) homepage.
* [ADD] Improved user feedback: Statusbar notifications, message boxes, reporting progress status in multiple ways.
* [CHG] Window now starts in the center of the screen.
* [ADD] Added translation strings.
* [FIX] Improved keyboard navigation.
* [FIX] Fixed threading issues that caused crashes.


1.0.0 (2006-03-28)
------------------
* [ADD] Added radio-buttons ("empty device").
* [ADD] Added capability for ejecting the target device when the job is done.
* [CHG] Changed GUI: Now using a tab-view/control.
* [FIX] Fixed 'blank-status'-bug (occured while the first file was being copied).


0.0.0 (ca. 2005-08-25)
----------------------
* [INF] Started the project ('bottom-up': implementing the playlist, detecting USB devices, etc. before working on GUI issues).

____________________________________________________________________________________________________
[ADD]: Added new feature.
[CHG]: Changed existing functionality.
[DEP]: Deprecated (soon to be removed feature).
[REM]: Removed feature.
[FIX]: Fixed bug or issue.
[VUL]: Security vulnerability.
[INF]: General information.