# ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
# │                                            RandFill                                            │
# └────────────────────────────────────────────────────────────────────────────────────────────────┘
# Copyright © 2015 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license. See accompanied file "License.txt" for details.

# And here we convert the string back into a CMake list.
separate_arguments(_HELP_FILES WINDOWS_COMMAND ${HELP_FILES})

# Create proper HTML files by substituting the CMake variables.
foreach (ITEM ${_HELP_FILES})
	configure_file (${CMAKE_CURRENT_LIST_DIR}/${ITEM}.in ${CMAKE_CURRENT_BINARY_DIR}/help/${ITEM} @ONLY)
endforeach ()

# Copy image files (screenshots).
file (COPY ${CMAKE_CURRENT_LIST_DIR} DESTINATION ${CMAKE_CURRENT_BINARY_DIR} FILES_MATCHING PATTERN "*.png")
