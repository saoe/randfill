#
# Function to return the full path to the installation file for the Microsoft Visual C++ Redistributable Package.
#
# CPU_ARCHITECTURE should be either "x86" or "x64".
# OUTVAR is for the return value; it will hold something like "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Redist/MSVC/14.16.27012/vcredist_x64.exe"
#
# Sascha Offe on 2019-08-06
# Inspired by https://github.com/Cockatrice/Cockatrice/blob/master/cmake/FindVCredistRuntime.cmake
#
function (FindVCRedist CPU_ARCHITECTURE OUTVAR)
	if (MSVC AND WIN32)
		set (VCREDIST_FOUND    "NO")
		set (VCREDIST_FILENAME "vcredist_${CPU_ARCHITECTURE}.exe")
		
		set (CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_SKIP "TRUE") # Don't copy the DLLs when the install target is run.
		
		include (InstallRequiredSystemLibraries)

		# Found at least one element? Then use it to get the path.
		list (LENGTH CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS count) 
		
		if (count GREATER 0)
			list (GET CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS 0 VCREDIST_DIRECTORY)

			# Get the path to the installation *.exe, not the (*.dll) files.
			get_filename_component (VCREDIST_DIRECTORY ${VCREDIST_DIRECTORY} DIRECTORY)
			get_filename_component (VCREDIST_DIRECTORY ${VCREDIST_DIRECTORY}/../../ ABSOLUTE)

			if (EXISTS "${VCREDIST_DIRECTORY}/${VCREDIST_FILENAME}")
				set (VCREDIST_FOUND "YES")
				set (VCREDIST_PATH  ${VCREDIST_DIRECTORY}/${VCREDIST_FILENAME})
			endif ()
		endif ()

		if (VCREDIST_FOUND)
			# Return value by handing it to the 'out variable' and passing that to the parent scope (= calling script).
			set (${OUTVAR} ${VCREDIST_PATH} PARENT_SCOPE)
		else()
			message (WARNING "Could not find Microsoft Visual C++ Redistributable Package.")
		endif ()
	endif ()
endfunction ()