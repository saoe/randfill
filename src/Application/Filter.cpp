// Copyright 2005-2014 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.

#include <cstring>
#include <fstream>

#include <windows.h>
#include <commctrl.h>

#include "Filter.h"
#include "resource.h"

//extern std::ofstream logfile;

using std::string;
using std::map;


// -------------------------------------------------------------------------------------------------
//

Playlist* Filter::apply (Playlist* source)
{
	Playlist* target = new Playlist;
	
	int x = source->getNumberOfEntries();
	
	// Iterate over the playlist.
	for (int i = 0; i < x; ++i)
	{
		// Iterate over the ruleset.
		for (map<string,bool>::iterator pos = rules.begin(); pos != rules.end(); ++pos)
		{
			if (strstr(source->getFilename(i), pos->first.c_str()) || strstr(source->getDirectory(i), pos->first.c_str()))
			{
				if (pos->second)
				{
					source->copyFile(target, i);
					goto LABEL_NEXT;
				}
				else
				{
					goto LABEL_NEXT;
				}
			}	
			else
			{
				continue;
			}
		}
		
		if (accept_all)
			source->copyFile(target, i);

		LABEL_NEXT: ; // Needed for fast exit.
	}
	return target;
}


// -------------------------------------------------------------------------------------------------
//

void Filter::addRule (string subject, bool accept)
{
	rules.insert(std::make_pair(subject, accept));
}


// -------------------------------------------------------------------------------------------------
//

void Filter::addRule (char* subject, bool accept)
{
	rules.insert(std::make_pair(string(subject), accept));
}


// -------------------------------------------------------------------------------------------------
//

void Filter::removeRule (string subject)
{
	map<string, bool>::iterator pos = rules.find(subject);
	
	if (pos != rules.end())
		rules.erase(pos);
}


// -------------------------------------------------------------------------------------------------
//

void Filter::removeRule (char* subject)
{
	map<string, bool>::iterator pos = rules.find(subject);

	if (pos != rules.end())
		rules.erase(pos);
}


// -------------------------------------------------------------------------------------------------
//

void Filter::clear ()
{
	rules.clear();
}


// -------------------------------------------------------------------------------------------------
//

void Filter::acceptAll ()
{
	accept_all = true;
}


// -------------------------------------------------------------------------------------------------
//

void Filter::rejectAll ()
{
	accept_all = false;
}

// -------------------------------------------------------------------------------------------------
//

bool Filter::queryAcceptAll () const
{
	return accept_all;
}

// -------------------------------------------------------------------------------------------------
//

void Filter::updateList (HWND h)
{
	SendMessage(h, LVM_DELETEALLITEMS, 0, 0);
	
	LVITEM lvi;
	
	lvi.mask = LVIF_TEXT; 
	
	int index = 0;
	for (map<string, bool>::iterator pos = rules.begin(); pos != rules.end(); ++pos)
	{
   		lvi.iItem = index;
		lvi.iSubItem = 0;
		lvi.pszText = const_cast<char*>(pos->second ? "Accept" : "Reject");
		SendMessage(h, LVM_INSERTITEM, 0, (LPARAM)&lvi);
		
		//lvi.iItem = index;
		lvi.iSubItem = 1;
		lvi.pszText = const_cast<char*>(pos->first.c_str());
		SendMessage(h, LVM_SETITEM, 0, (LPARAM)&lvi);
		
		++index;		
	}
}


// -------------------------------------------------------------------------------------------------
//

bool Filter::isAccepted (string s)
{
	map<string, bool>::iterator pos = rules.find(s);
	
	if (pos != rules.end())
		return pos->second;
	
	// FIXME: What to do/return if nothing is found? 'false' is already a valid value...
}


// -------------------------------------------------------------------------------------------------
//

bool Filter::isEmpty () const
{
	return rules.empty();
}
