// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#include <shellapi.h>
#include <commctrl.h>
#include <shlwapi.h>        // PathFileExists()

#include "Application.h"
#include "Playlist.h"

#include <QtCore/QDirIterator>
#include <QtCore/QTextStream>

extern Application app;

// -------------------------------------------------------------------------------------------------
// Default constructor.

Playlist::Playlist () : number_of_files_on_playlist(0), playlist_size(0)
{
}

// -------------------------------------------------------------------------------------------------
// Copy constructor.

Playlist::Playlist (const Playlist& original_object)
{
	playlist_size               = original_object.playlist_size;
	number_of_files_on_playlist = original_object.number_of_files_on_playlist;
	list_of_directories         = original_object.list_of_directories;
	
	for (int i = 0; i < number_of_files_on_playlist; ++i)
	{
		entry* e = new entry;

		e->directory = original_object.pl[i]->directory;
		e->filename  = original_object.pl[i]->filename;
						
		e->filesize = original_object.pl[i]->filesize;
		e->marked   = original_object.pl[i]->marked;
		
		pl.push_back(e);
	}
}


// -------------------------------------------------------------------------------------------------
// Destructor.

Playlist::~Playlist ()
{
	clear();
}


// -------------------------------------------------------------------------------------------------
// Assignment operator

Playlist& Playlist::operator= (const Playlist& right)
{
	if (this != &right)
	{
		clear();
		playlist_size               = right.playlist_size;
		number_of_files_on_playlist = right.number_of_files_on_playlist;
		list_of_directories         = right.list_of_directories;
		
		for (int j = 0; j < number_of_files_on_playlist; ++j)
		{
			entry* e = new entry;
			
			e->directory = right.pl[j]->directory;
			e->filename  = right.pl[j]->filename;
					
			e->filesize = right.pl[j]->filesize;
			e->marked   = right.pl[j]->marked;

			pl.push_back(e);
		}
	}
	return *this;
}


// -------------------------------------------------------------------------------------------------
// Clears the playlist (zero entries).

void Playlist::clear ()
{
	for (std::vector<entry*>::iterator i = pl.begin(); i != pl.end(); ++i)
		delete *i;
	
	pl.clear();
	list_of_directories.clear();
	number_of_files_on_playlist = 0;
}


// -------------------------------------------------------------------------------------------------
// Copies a single entry for a file from one playlist-object to another.

void Playlist::copyFile (Playlist* target, int i)
{
	entry* e = new entry;
	
	e->directory = this->pl[i]->directory;
	e->filename  = this->pl[i]->filename;
		
	e->filesize = this->pl[i]->filesize;
	e->marked   = this->pl[i]->marked;
	
	target->playlist_size               += this->pl[i]->filesize;
	target->number_of_files_on_playlist += 1;
	
	target->pl.push_back(e);	
}


// -------------------------------------------------------------------------------------------------
// Removes a single entry for a file from a playlist.

void Playlist::removeFile (std::vector<entry*>::iterator i)
{
	playlist_size -= (*i)->filesize;
	number_of_files_on_playlist--;
	delete *i;
	pl.erase(i);
}


// -------------------------------------------------------------------------------------------------
// Removes a single entry for a file from a playlist.

void Playlist::removeFile (std::vector<entry*>::reverse_iterator i)
{
	playlist_size -= (*i)->filesize;
	number_of_files_on_playlist--;
	delete *i;
	pl.erase(--(i.base())); // - Convert it, because *.erase() only accepts 'normal' iterators.
	                        // - Decrement it, because a converted iterator still points to the same "element" as before, but not to the same "value".
	                        //   (Read it up in your documentation for the C++ standard library, or Josuttis' book [7.4.1 Rev. Iter.])
}


// -------------------------------------------------------------------------------------------------
// Removes the files of directory "dir" from the playlist and also removes the entry for the directory itself.

void Playlist::removeDirectory (const QString dir)
{
	// Loop backwards (Otherwise the iterator gets invalidated, skips files after the removal of an item).
	for (std::vector<entry*>::reverse_iterator i = pl.rbegin(); i != pl.rend(); ++i)
	{
		if ((*i)->directory == dir)
			removeFile(i);
	}
	
	std::set<QString>::iterator j = list_of_directories.find(dir);

	if (j != list_of_directories.end())
		list_of_directories.erase(j);
}


// -------------------------------------------------------------------------------------------------
// Adds (recursivly) the files of a directory to the playlist.

int Playlist::addDirectory (const QString path, bool include_subdirectories)
{
	QDirIterator diriter(path, (include_subdirectories ? QDirIterator::Subdirectories | QDirIterator::FollowSymlinks : QDirIterator::NoIteratorFlags));

	if (diriter.hasNext())
	{
		do
		{
			diriter.next();
			
			if (!diriter.fileInfo().isDir())
			{
				entry* e = new entry;

				// 'directory' gets a trailing backslash (on Windows) for easier path-building later on.
				e->directory = QDir::toNativeSeparators(diriter.fileInfo().absolutePath()) + QDir::separator();
				e->filename  = diriter.fileInfo().fileName();
				e->filesize  = diriter.fileInfo().size();
				e->marked    = false;
						
				playlist_size += e->filesize;
				
				pl.push_back(e);
				
				++number_of_files_on_playlist;
				list_of_directories.insert(e->directory);
			}
		}
		while (diriter.hasNext());
	}
	else
	{
		list_of_directories.clear();
	}

	return list_of_directories.size(); // For a std::set, this is indeed size(), not count().
}


// -------------------------------------------------------------------------------------------------
// Unmarks all entries.

void Playlist::unmark ()
{
	for (int i = 0; i < number_of_files_on_playlist; ++i)
		pl[i]->marked = false;
}


// -------------------------------------------------------------------------------------------------
// Returns the directory component.

const QString Playlist::getDirectory (int i) const
{
	return pl[i]->directory;
}


// -------------------------------------------------------------------------------------------------
//

const QString Playlist::getFilename (int i) const
{
	return pl[i]->filename;
}


// -------------------------------------------------------------------------------------------------
// Returns a (read-only) set (list with no duplicated items) of all directories
// from which the files on the playlist come.

const std::set<QString> Playlist::getDirectories () const
{
	return list_of_directories;
}


// -------------------------------------------------------------------------------------------------
// Returns the count of entries on the playlist.

int Playlist::getNumberOfEntries () const
{
	return number_of_files_on_playlist;
}


// -------------------------------------------------------------------------------------------------
// Returns a random integer in the range [0, n)
//
// Based on an algorithm from "Accelerated C++", section 7.4.4/page 135,
// resp. http://groups.google.de/group/comp.lang.c++.moderated/msg/01ed2f91aac1eb59?hl=de&

int Playlist::generateRandomNumber (int n)
{
	//if (n <= 0 || n > RAND_MAX)
		//throw domain_error("Argument to nrand is out of range");

	const int bucket_size = RAND_MAX / n;
	int r;

	do
	{
		r = ::rand() / bucket_size;     // <-- cstdlib's rand()!
	}
	while (r >= n);

	return r;
} 


// -------------------------------------------------------------------------------------------------
// Copies the files listed by the playlist to a storage device.

void Playlist::copy (DeviceManager dm, int index)
{
	srand(time(0)); // Seed for the (pseudo-)random-number-generator
	
	char dest [MAX_PATH + 1] = "_:\\";
	dest[0] = dm.getDriveletter(index);

	QFile* playlistfile = NULL;

	if (DeviceManager::isTargetDirectoryEmpty() || !dm.use_directory)
	{
		if (app.create_playlist_file)
		{
			QString basepath(dest);
			// QFile expects the file separator to be '/' regardless of operating system.
			// The use of other separators (e.g., '\') is not supported. Thus:
			basepath.replace('\\','/');
			playlistfile = new QFile(basepath+"/"+app.playlist_filename+"."+app.playlist_fileformat);
		}

		strcat(dest, "\0"); // double-zero-terminated for SHFileOperation().		
	}
	else
	{
		QString temp = temp.fromStdWString(DeviceManager::getTargetDirectory());
		strcat(dest, temp.toLocal8Bit());
		
		if (!PathFileExistsA(dest))       // -A: Calls explicitly the ASCII version!
			CreateDirectoryA(dest, NULL); // -A: Calls explicitly the ASCII version!

		if (app.create_playlist_file)
		{
			QString basepath(dest);
			// QFile expects the file separator to be '/' regardless of operating system.
			// The use of other separators (e.g., '\') is not supported. Thus:
			basepath.replace('\\','/');
			playlistfile = new QFile(basepath+"/"+app.playlist_filename+"."+app.playlist_fileformat);
		}

		strcat(dest, "\\\0");
	}

	if (playlistfile != NULL)
	{
		playlistfile->open(QIODevice::WriteOnly | QIODevice::Text);
		preparePlaylistFile(playlistfile, app.playlist_fileformat);
	}
	
	unsigned long byte_limit   = 0;
	unsigned long byte_counter = 0;
	
	if (dm.max_percent == 100)
		byte_limit = dm.getFreeCapacity(index, cuBYTE);
	else
		byte_limit = (dm.getFreeCapacity(index, cuBYTE) / 100) * dm.max_percent;
		
	char src_buffer [MAX_PATH + 1] = "\0";
	char dst_buffer [MAX_PATH + 1] = "\0";	

	SHFILEOPSTRUCTA sfos; // -A: Is explicitly the ASCII version!
	sfos.hwnd   = NULL;
	sfos.wFunc  = FO_COPY;
	sfos.fFlags = FOF_RENAMEONCOLLISION | FOF_SILENT | FOF_NORECURSION;
	
	int c = this->getNumberOfEntries();
	
	int i = 0, filecount = 0, percent = 0, fileop = 0;
	
	while (i < c)
	{   		
		if (app.stop_copy_thread != true)
		{
			int r = generateRandomNumber(c);
		
			if (this->pl[r]->marked == false)
			{
				if ((byte_counter + this->pl[r]->filesize) <= byte_limit)
				{
					strcpy(src_buffer, this->pl[r]->directory.toLatin1());
					strcat(src_buffer, this->pl[r]->filename.toLatin1());
					int sb_len = strlen(src_buffer);
					src_buffer[sb_len + 1] = '\0'; // SHFileOperation() requires a second zero-terminator.

					strcpy(dst_buffer, dest);
					strcat(dst_buffer, this->pl[r]->filename.toLocal8Bit());
				
					sfos.pFrom = src_buffer;
					sfos.pTo   = dst_buffer;

					this->pl[r]->marked = true;
					byte_counter += this->pl[r]->filesize;
					
					percent = (byte_counter >> 10) * 100 / (byte_limit >> 10); // Rationale: Without shortening the numbers (shift-div.), they grow too big and roll-over, messing up the percent calculation.

					fileop = SHFileOperationA(&sfos); // -A: Calls explicitly the ASCII version!

					if (fileop || sfos.fAnyOperationsAborted)
					{
						emit CopyingAborted(fileop);
						goto Leave;
					}
					else
					{
						if (app.create_playlist_file)
						{
							updatePlaylistFile (playlistfile, app.playlist_fileformat, this->pl[r]->filename, i);
							byte_counter += playlistfile->size();
						}

						++filecount;
						++i;
					
						emit CopyingStatus(percent, filecount, this->pl[r]->filename);
					}
				}
				else
				{
					this->pl[r]->marked = true; // Mark it so that we won't visit it again
					++i;                        // Increase the counter to prevent an infinite loop.
				}
			}
		}
		else
		{
			// The user has signaled a cancel-event. Time to leave.
			// Negative return value means: Job has been aborted.
			emit CopyingAbortedByUser();
			goto Leave;
		}
	}

	finishPlaylistFile(playlistfile, app.playlist_fileformat, filecount);
	emit CopyingDone(filecount);

	Leave:
	// Reset and shuffle, in case the users runs the same playlist again.	
	unmark();
	shuffle();
}

// -------------------------------------------------------------------------------------------------
// Further handling of the playlist file.

void Playlist::preparePlaylistFile (QFile* file, QString format)
{
	if (format.toLower() == "pls")
	{
		QTextStream out(file);
		out << "[playlist]" << '\n';
	}
}

void Playlist::updatePlaylistFile (QFile* file, QString format, QString text, int index)
{
	if (format.toLower() == "m3u")
	{
		QTextStream out(file);
		out << text << '\n';
		return;
	}

	if (format.toLower() == "pls")
	{
		QTextStream out(file);
		out << "File" << index << "=" << text << '\n';
		out << "Title" << index << "=" << text << '\n';
		out << "Length" << index << "=" << "-1" << '\n'; // Length in seconds, currently ignored.
		return;
	}
}

void Playlist::finishPlaylistFile (QFile* file, QString format, int count)
{
	if (file != NULL)
	{
		if (format.toLower() == "pls")
		{
			QTextStream out(file);
			out << "NumberOfEntries=" << count << '\n';
			out << "Version=2" << '\n';
		}

		file->close();
		delete file;
	}
}

// -------------------------------------------------------------------------------------------------
// Shuffles the list of files (simple algorithm, but gets the job done).

void Playlist::shuffle ()
{
	int c = this->getNumberOfEntries();
	
	for (int i = 0; i < c; i++)
	{
		int r = generateRandomNumber(c);
		std::swap(pl[i], pl[r]);
	}
}
