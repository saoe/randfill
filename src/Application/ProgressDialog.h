// ☺
// Copyright © 2014-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QtWidgets/QDialog>

#include "CopyThread.h"

class QListWidget;
class QProgressBar;
class QLabel;
class QPushButton;
class QVBoxLayout;

class ProgressDialog : public QDialog
{
	Q_OBJECT

	public:
		ProgressDialog (int target_index);
		
		int progress_status; // Caller of the dialog gets this as return value.

	public slots:
		void OnCopyingStatus (int percent, int filecount, QString filename);
		void OnCopyingAborted (int errorcode);
		void OnCopyingDone (int filecount);
		void OnCopyingAbortedByUser ();
		void OnControlButton ();
		void OnRejected ();

	private:
		CopyThread*   copythread;

		QDialog*      ProgressView;
		QListWidget*  List_Files;
		QProgressBar* ProgressBar_Progress;
		QLabel*       Label_Summary;
		QPushButton*  Button_AbortClose;
		QVBoxLayout*  Layout;

		QString text_windowtitle;
		QString text_abort;
		QString text_close;

		void updateTranslation ();
};

#endif
