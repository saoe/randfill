/*
┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                             RandFill                                             │
└──────────────────────────────────────────────────────────────────────────────────────────────────┘
Copyright © 2005-2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see accompanying file 'License.txt' for details).

This module creates and controls the main GUI window.
*/

#include <iostream>

#include <shlobj.h>
#include <shlwapi.h>
#include <dbt.h>

#include <QtWidgets/QMenuBar>
#include <QtWidgets/QLabel>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSlider>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSizePolicy>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtGUI/QFontMetrics>
#include <QtGUI/QDesktopServices>
#include <QtCore/QUrl>
#include <QtCore/QLocale>
#include <QtGUI/QCloseEvent>
#include <QtCore/QDebug>

#include "Application.h"
#include "Playlist.h"
#include "../DevicesLib/Device.h"
#include "../DevicesLib/DeviceManager.h"
#include "ProgressDialog.h"
#include "MainWindow.h"
#include "version.h"

// Load GUID Classes (usually located in a header file of the Windows Driver Kit (WDK)).
static GUID GUID_DEVINTERFACE_USB_HUB             = { 0xf18a0e88, 0xc30c, 0x11d0, {0x88, 0x15, 0x00, 0xa0, 0xc9, 0x06, 0xbe, 0xd8} };
static GUID GUID_DEVINTERFACE_USB_DEVICE          = { 0xA5DCBF10L, 0x6530, 0x11D2, { 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED } };
static GUID GUID_DEVINTERFACE_USB_HOST_CONTROLLER = { 0x3abf6f2d, 0x71c4, 0x462a, {0x8a, 0x92, 0x1e, 0x68, 0x61, 0xe6, 0xaf, 0x27}};

extern Application app;

// Constructor
MainWindow::MainWindow ()
{
	// ---- Setting-up the main window. ----
	MainWidget = new Widget;
	
	MainWidget->setWindowTitle(QString::fromStdWString(APP_NAME));
	MainWidget->setWindowIcon(QIcon(":/randfill.ico"));
	
	// ---- Setting up the menubar ----
	// Note that this program is using a central QWidget, not a QMainWindow;
	// the menubar therefore has to be prepared here and later added to the layout to be visible.
	QMenuBar* MenuBar = new QMenuBar();

	Menu_Language = new QMenu();
	MenuBar->addMenu(Menu_Language);

	QActionGroup* Menu_Language_ActionGroup = new QActionGroup(this);
	
	lang_AUTO = new QAction(this);
	lang_AUTO->setCheckable(true);
	lang_AUTO->setData("AUTO");  // Internal ID.
	Menu_Language->addAction(lang_AUTO);
	Menu_Language_ActionGroup->addAction(lang_AUTO);

	lang_EN = new QAction(this);
	lang_EN->setCheckable(true);
	lang_EN->setData("EN");      // Internal ID.
	Menu_Language->addAction(lang_EN);
	Menu_Language_ActionGroup->addAction(lang_EN);

	lang_DE = new QAction(this);
	lang_DE->setCheckable(true);
	lang_DE->setData("DE");      // Internal ID.
	Menu_Language->addAction(lang_DE);
	Menu_Language_ActionGroup->addAction(lang_DE);
	
	Menu_Help = new QMenu();
	Action_AboutDialog = new QAction(this);
	Menu_Help->addAction(Action_AboutDialog);
	connect(Action_AboutDialog, &QAction::triggered, this, &MainWindow::OnAboutDialog);

	Action_OpenHelp = new QAction(this);
	Menu_Help->addAction(Action_OpenHelp);
	connect(Action_OpenHelp, &QAction::triggered, this, &MainWindow::OnOpenHelp);
	MenuBar->addMenu(Menu_Help);

	connect(Menu_Language, &QMenu::triggered, this, &MainWindow::OnLanguageChanged);

	// ---- Creating the GUI elements for the 'Source' area. ----
	Group_Source = new QGroupBox();
		
	Label_SourcePath = new QLabel;
	Label_SourcePath->setFrameStyle(QFrame::Panel | QFrame::Sunken);

	Button_ChangePath   = new QPushButton();
	CheckBox_IncSubDir  = new QCheckBox();
	Button_AddPath      = new QPushButton();
	
	Label_ListHeader = new QLabel();
	List_Sources     = new QListWidget();
	List_Sources->setSelectionMode(QAbstractItemView::ExtendedSelection); // Allow multiple selection (with CTRL).

	Button_RemovePath = new QPushButton();
	Button_ClearAll   = new QPushButton();
	Button_OpenSource = new QPushButton();
	Button_Filter     = new QPushButton();

	CheckBox_PlaylistFile = new QCheckBox();
	LineEdit_PlaylistFileName = new QLineEdit;
	ComboBox_PlaylistFileFormat = new QComboBox;
	ComboBox_PlaylistFileFormat->addItems(*app.supported_playlistfile_formats);

	Layout_SourceGroupPlaylistFile = new QHBoxLayout;
	Layout_SourceGroupPlaylistFile->addWidget(CheckBox_PlaylistFile);
	Layout_SourceGroupPlaylistFile->addWidget(LineEdit_PlaylistFileName);
	Layout_SourceGroupPlaylistFile->addWidget(ComboBox_PlaylistFileFormat);
	
	Layout_SourceGroup1 = new QVBoxLayout;
	Layout_SourceGroup1->addWidget(Button_RemovePath);
	Layout_SourceGroup1->addWidget(Button_ClearAll);
	Layout_SourceGroup1->addWidget(Button_OpenSource);
	Layout_SourceGroup1->addStretch();
	// Layout_SourceGroup1->addWidget(Button_Filter); // Irrelevant, until the Filter functionality is reworked.

	GridLayout_Source = new QGridLayout(Group_Source);
	GridLayout_Source->addWidget(Label_SourcePath, 0, 0);
	GridLayout_Source->addWidget(Button_ChangePath, 0, 1);
	GridLayout_Source->addWidget(CheckBox_IncSubDir, 1, 0);
	GridLayout_Source->addWidget(Button_AddPath, 1, 1);
	GridLayout_Source->addWidget(Label_ListHeader, 2, 0);
	GridLayout_Source->addWidget(List_Sources, 3, 0);
	GridLayout_Source->addLayout(Layout_SourceGroup1, 3, 1);
	GridLayout_Source->addLayout(Layout_SourceGroupPlaylistFile, 4, 0);
		
	// ---- Creating the GUI elements for the 'Target' area. ----
	Group_Target = new QGroupBox();

	ComboBox_TargetDevice = new QComboBox();
	Button_OpenTarget = new QPushButton();

	Layout_TargetGroup3 = new QHBoxLayout;
	Layout_TargetGroup3->addWidget(ComboBox_TargetDevice);
	Layout_TargetGroup3->addWidget(Button_OpenTarget);
	Layout_TargetGroup3->setStretchFactor(ComboBox_TargetDevice, 80);
	Layout_TargetGroup3->setStretchFactor(Button_OpenTarget, 20);
	
	Label_TargetCapacity = new QLabel();
	Label_TargetCapacity->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	Label_TargetCapacity->setMargin(3);

	Layout_TargetGroup1 = new QHBoxLayout;
	Layout_TargetGroup1->addWidget(Label_TargetCapacity);

	Label_CapacityLimit = new QLabel();

	SpinBox_CapacityLimit = new QSpinBox();
	SpinBox_CapacityLimit->setRange(0, 100); // Percentage.
	SpinBox_CapacityLimit->setFixedSize(SpinBox_CapacityLimit->sizeHint());
	
	Slider_CapacityControl = new QSlider(Qt::Horizontal);
	Slider_CapacityControl->setRange(0, 100); // Percentage.
		
	Layout_TargetGroup2 = new QHBoxLayout;
	Layout_TargetGroup2->addWidget(Label_CapacityLimit, 1);
	Layout_TargetGroup2->addWidget(SpinBox_CapacityLimit, 1);
	Layout_TargetGroup2->addWidget(Slider_CapacityControl, 1);

	CheckBox_UseDir  = new QCheckBox();
	LineEdit_DirName = new QLineEdit;
	
	CheckBox_EmptyDeviceBeforeCopying = new QCheckBox();

	ButtonGroup_EmptyDeviceBeforeCopying = new QButtonGroup;
	ButtonGroup_EmptyDeviceBeforeCopying->setExclusive(true);
	
	RadioButton_DeleteAllFiles = new QRadioButton();
		
	Label_DeleteAllFiles = new QLabel;
	Label_DeleteAllFiles->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	Label_DeleteAllFiles->setMargin(3);
	
	RadioButton_DeleteOnlyCertainFiletypes = new QRadioButton();
	ButtonGroup_EmptyDeviceBeforeCopying->addButton(RadioButton_DeleteAllFiles);
	ButtonGroup_EmptyDeviceBeforeCopying->addButton(RadioButton_DeleteOnlyCertainFiletypes);

	LineEdit_Filetypes = new QLineEdit;
	CheckBox_EjectDevice = new QCheckBox();

	GridLayout_Target = new QGridLayout;
	GridLayout_Target->setColumnMinimumWidth(0, 16);
	GridLayout_Target->addWidget(CheckBox_UseDir, 0, 0, 1, 2);
	GridLayout_Target->addWidget(LineEdit_DirName, 0, 2);
	GridLayout_Target->addWidget(CheckBox_EmptyDeviceBeforeCopying, 1, 0, 1, 3);
	GridLayout_Target->addWidget(RadioButton_DeleteAllFiles, 2, 1);
	GridLayout_Target->addWidget(Label_DeleteAllFiles, 2, 2);
	GridLayout_Target->addWidget(RadioButton_DeleteOnlyCertainFiletypes, 3, 1);
	GridLayout_Target->addWidget(LineEdit_Filetypes, 3, 2);
	GridLayout_Target->addWidget(CheckBox_EjectDevice, 4, 0, 1, 3);		
	
	Layout_TargetGroup0 = new QVBoxLayout(Group_Target);
	Layout_TargetGroup0->addLayout(Layout_TargetGroup3);
	Layout_TargetGroup0->addLayout(Layout_TargetGroup1);
	Layout_TargetGroup0->addLayout(Layout_TargetGroup2);
	Layout_TargetGroup0->addLayout(GridLayout_Target);

	Button_OK = new QPushButton();

	setLanguage(app.language_mode); // Initializing the language mode.

	// ---- Putting the whole layout together. ----
	MainLayout = new QVBoxLayout;
	MainLayout->setMenuBar(MenuBar);
	MainLayout->addWidget(Group_Source);
	MainLayout->addWidget(Group_Target);
	MainLayout->addWidget(Button_OK, 0, Qt::AlignCenter);
			
	MainWidget->setLayout(MainLayout);

	// ---- Setting up (most) signal/slot connections for source & target ----
	
	// Source
	connect(Button_ChangePath, &QPushButton::clicked, this, &MainWindow::OnChangeSource);
	connect(CheckBox_IncSubDir, &QCheckBox::toggled, this, &MainWindow::OnToggleIncsubdir);
	connect(CheckBox_PlaylistFile, &QCheckBox::toggled, this, &MainWindow::OnTogglePlaylistFile);
	connect(LineEdit_PlaylistFileName, &QLineEdit::textEdited, this, &MainWindow::OnUpdatePlaylistFileName);
	connect(ComboBox_PlaylistFileFormat, qOverload<const QString&>(&QComboBox::currentIndexChanged), this, &MainWindow::OnUpdatePlaylistFileFormat);
	connect(Button_AddPath, &QPushButton::clicked, this, &MainWindow::addSource);
	connect(Button_RemovePath, &QPushButton::clicked, this, &MainWindow::removeSource);
	connect(Button_ClearAll, &QPushButton::clicked, this, &MainWindow::clearSource);
	connect(List_Sources, &QListWidget::itemSelectionChanged, this, &MainWindow::OnChangeSourceSelection);
	connect(Button_OpenSource, &QPushButton::clicked, this, &MainWindow::openSourceInExplorer);
	connect(this, &MainWindow::SourceChanged, this, &MainWindow::OnSourceChanged);
	
	// Target
	connect(ComboBox_TargetDevice, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::OnTargetChanged);
	connect(this, qOverload<int>(&MainWindow::TargetChanged), this, &MainWindow::OnTargetChanged);
	connect(ComboBox_TargetDevice, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::updateLabelWhenTargetChanged);
	connect(Button_OpenTarget, &QPushButton::clicked, this, &MainWindow::openTargetInExplorer);

	connect(SpinBox_CapacityLimit, qOverload<int>(&QSpinBox::valueChanged), Slider_CapacityControl, &QSlider::setValue);
	connect(SpinBox_CapacityLimit, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::OnUseCapacity);
	connect(Slider_CapacityControl, qOverload<int>(&QSlider::valueChanged), SpinBox_CapacityLimit, &QSpinBox::setValue);
	connect(Slider_CapacityControl, qOverload<int>(&QSlider::valueChanged), this, &MainWindow::OnUseCapacity);

	connect(CheckBox_UseDir, &QCheckBox::toggled, this, &MainWindow::OnToggleUseDirectory);
	connect(CheckBox_UseDir, &QCheckBox::toggled, this, &MainWindow::updateLabelWhenTargetChanged);
	connect(LineEdit_DirName, &QLineEdit::textEdited, this, &MainWindow::OnUpdateTargetDirectory);
	connect(LineEdit_DirName, &QLineEdit::textEdited, this, &MainWindow::updateLabelWhenTargetChanged);
	
	connect(CheckBox_EmptyDeviceBeforeCopying, &QCheckBox::toggled, this, &MainWindow::OnToggleClearDevice);
	connect(RadioButton_DeleteAllFiles, &QRadioButton::toggled, this, &MainWindow::OnToggleDeleteAllFiles);
	connect(RadioButton_DeleteOnlyCertainFiletypes, &QRadioButton::toggled, this, &MainWindow::OnToggleDeleteOnlyCertainFiletypes);
	connect(CheckBox_EjectDevice, &QCheckBox::toggled, this, &MainWindow::OnToggleEjectDevice);

	connect(LineEdit_Filetypes, &QLineEdit::textEdited, this, &MainWindow::OnUpdateFiletypesToDelete);

	connect(Button_OK, &QPushButton::clicked, this, &MainWindow::beginCopying);
	
	// Make main window non-resizable.
	// MainWidget->setFixedHeight(MainWidget->sizeHint().height());
	// MainWidget->setFixedWidth(MainWidget->sizeHint().width());

	// ---- Set the GUI values accordingly to the default settings/configuration file. ----

	// Button_Filter->setEnabled(false); // Irrelevant, until the Filter functionality is reworked.

	Button_OpenSource->setEnabled(false);
		
	CheckBox_IncSubDir->setChecked(app.include_subdirectory);
	
	CheckBox_PlaylistFile->setChecked(app.create_playlist_file);
	LineEdit_PlaylistFileName->setEnabled(app.create_playlist_file);
	LineEdit_PlaylistFileName->setText(app.playlist_filename);
	ComboBox_PlaylistFileFormat->setEnabled(app.create_playlist_file);
	OnUpdatePlaylistFileFormat(app.playlist_fileformat);

	Slider_CapacityControl->setValue(app.dm.max_percent);

	CheckBox_UseDir->setChecked(app.dm.use_directory);
	LineEdit_DirName->setText(QString::fromStdWString(DeviceManager::getTargetDirectory()));
	LineEdit_DirName->setEnabled(app.dm.use_directory);

	CheckBox_EmptyDeviceBeforeCopying->setChecked(app.dm.clear_before_copying);
	OnToggleClearDevice(app.dm.clear_before_copying);
	LineEdit_Filetypes->setText(QString::fromStdWString(DeviceManager::getFiletypesToDeleteAsText()));
		
	CheckBox_EjectDevice->setChecked(app.dm.eject_when_done);

	// Registering this window for getting notified when (USB) devices are plugged in or are being removed.
	hDevNotify = NULL;
    registerForDeviceNotification();

	// Set up the worker thread that watches the target device for any file/directory changes.
	watch_target_worker.moveToThread(&watch_target_thread);
	connect(&watch_target_thread, &QThread::started, &watch_target_worker, &WatchTargetWorker::work);
	connect(&watch_target_thread, &WatchTargetThread::pauseThread, &watch_target_worker, &WatchTargetWorker::pause, Qt::DirectConnection);
	connect(&watch_target_thread, &WatchTargetThread::resumeThread, &watch_target_worker, &WatchTargetWorker::resume, Qt::DirectConnection);
	connect(this, qOverload<QString>(&MainWindow::TargetChanged), &watch_target_worker, &WatchTargetWorker::OnTargetChanged, Qt::DirectConnection);
	connect(&watch_target_worker, &WatchTargetWorker::ChangeNotification, this, &MainWindow::OnTargetChangeNotification);
	connect(&watch_target_worker, &WatchTargetWorker::finished, &watch_target_thread, &QThread::quit);
	connect(&watch_target_worker, &WatchTargetWorker::finished, &watch_target_worker, &QObject::deleteLater);
	connect(&watch_target_thread, &WatchTargetThread::finished, &watch_target_thread, &QObject::deleteLater);
	watch_target_thread.start();

	// Initial check at program start if any (removable storage) device is plugged in.
	// Then emit a signal to kick-off the enabling/disabling of the controls.
	app.dm.scanForDevices();
	emit TargetChanged(0);
	emit SourceChanged();

	MainWidget->show();

	// Needs to be done after showing the widgets; otherwise size() reports a wrong value.
	Label_SourcePath->setText(QDir::toNativeSeparators(truncatePath(app.source_path, Label_SourcePath->width())));
}


// Destructor
MainWindow::~MainWindow ()
{
	if (NULL != hDevNotify)
		UnregisterDeviceNotification(hDevNotify);
}


// What to do if a device is plugged-in or removed from the system.
void MainWindow::OnDeviceChange (WPARAM wParam, LPARAM lParam)
{
	// Handling WM_DEVICECHANGE.
	switch (wParam)
	{
		case DBT_DEVICEARRIVAL:
			if (app.dm.onDeviceArrival((PDEV_BROADCAST_HDR) lParam))
			{
				emit TargetChanged(0);
			}
			break;
		
		case DBT_DEVICEREMOVECOMPLETE:
			if (app.dm.onDeviceRemoval((PDEV_BROADCAST_HDR) lParam))
			{
				emit TargetChanged(0);
			}
			break;
	}
}


void MainWindow::OnUseCapacity (int value)
{
	app.dm.max_percent = value;
}


// Browse for folder (Source view).
void MainWindow::OnChangeSource ()
{
	QString path = app.source_path.isEmpty() == true ? QDir::homePath() : app.source_path;
	QString selected_directory = QFileDialog::getExistingDirectory(MainWidget, String_OnChangeSource, path, QFileDialog::ShowDirsOnly);
		
	if (!selected_directory.isEmpty())
	{
		app.source_path = QDir::toNativeSeparators(selected_directory);
		Label_SourcePath->setText(truncatePath(app.source_path, Label_SourcePath->width()));
		emit OnSourceChanged();
	}
}


// Invoked when the selection of source directory changes.
void MainWindow::OnChangeSourceSelection ()
{
	int n = List_Sources->selectedItems().count();
	
	// Enabling Explorer button when exactly one item (path) is selected.
	n == 1 ? Button_OpenSource->setEnabled(true) : Button_OpenSource->setEnabled(false);

	// Enabling Remove button when at least one item (path) is selected.
	n > 0 ? Button_RemovePath->setEnabled(true) : Button_RemovePath->setEnabled(false);
}


void MainWindow::OnToggleIncsubdir (bool state)
{
	app.include_subdirectory = state;
}


void MainWindow::addSource ()
{
	if (!app.source_path.isEmpty())
	{
		if (!app.pl.addDirectory(app.source_path, app.include_subdirectory))
		{
			QMessageBox::information(this, tr("Information"), tr("No files found in %1!\nTry to include the sub-directories or select another directory.").arg(app.source_path));
		}

		refreshSourceView(app.pl);
	}
}


void MainWindow::removeSource ()
{
	QList<QListWidgetItem*> selected_items = List_Sources->selectedItems();
	
	// Walk the list backward. Otherwise, after deleting an item, the indices change and will be invalid.
	for (int i = selected_items.count() - 1; i >= 0; i--) 
	{
		app.pl.removeDirectory(selected_items.at(i)->text());
		delete selected_items.at(i); // It's OK, that's the advised method the remove a QListWidgetItem.
	}

	refreshSourceView(app.pl);
}


void MainWindow::clearSource ()
{
	app.pl.clear();
	refreshSourceView(app.pl);
}


void MainWindow::openSourceInExplorer ()
{
	// Only open one Explorer window at a time.
	if (1 == List_Sources->selectedItems().count())
	{
		QList<QListWidgetItem*> selected_items = List_Sources->selectedItems();
		QDesktopServices::openUrl(QUrl::fromLocalFile(selected_items.at(0)->text()));
	}
}


void MainWindow::openTargetInExplorer ()
{
	int i = getTargetDevice();
	
	if (i != -1)
	{
		QString path(QString(app.dm.getDriveletter(i)) + ":");
		QDesktopServices::openUrl(QUrl::fromLocalFile(path));
	}
}


void MainWindow::OnToggleUseDirectory (bool state)
{
	app.dm.use_directory = state;
	LineEdit_DirName->setEnabled(state);
}


void MainWindow::OnToggleClearDevice (bool state)
{
	RadioButton_DeleteAllFiles->setEnabled(state);
	RadioButton_DeleteOnlyCertainFiletypes->setEnabled(state);

	RadioButton_DeleteAllFiles->setChecked(app.dm.clear_before_copying_all);
	RadioButton_DeleteOnlyCertainFiletypes->setChecked(app.dm.clear_before_copying_filetypes);
	
	if (state)
	{
		app.dm.clear_before_copying = state;

		if (app.dm.clear_before_copying_all)
		{
			RadioButton_DeleteAllFiles->setChecked(true);
			Label_DeleteAllFiles->setEnabled(true);
			RadioButton_DeleteOnlyCertainFiletypes->setChecked(false);
			LineEdit_Filetypes->setEnabled(false);
		}
		else
		{
			RadioButton_DeleteAllFiles->setChecked(false);
			Label_DeleteAllFiles->setEnabled(false);
			RadioButton_DeleteOnlyCertainFiletypes->setChecked(true);
			LineEdit_Filetypes->setEnabled(true);
		}
	}
	else
	{
		app.dm.clear_before_copying = state;
		Label_DeleteAllFiles->setEnabled(state);
		LineEdit_Filetypes->setEnabled(state);
	}
}


void MainWindow::OnToggleDeleteAllFiles (bool state)
{
	app.dm.clear_before_copying_all = state;
	app.dm.clear_before_copying_filetypes = !state;
	Label_DeleteAllFiles->setEnabled(state);
}


void MainWindow::updateLabelWhenTargetChanged ()
{
	if (getTargetDevice() != -1)
	{
		if (app.dm.use_directory)
			Label_DeleteAllFiles->setText(QString(app.dm.getDriveletter(getTargetDevice())) + ":\\" + LineEdit_DirName->text());
		else
			Label_DeleteAllFiles->setText(QString(app.dm.getDriveletter(getTargetDevice())) + ":\\");
	}
	else
	{
		Label_DeleteAllFiles->clear();
	}
}


void MainWindow::OnToggleDeleteOnlyCertainFiletypes (bool state)
{
	app.dm.clear_before_copying_all = !state;
	app.dm.clear_before_copying_filetypes = state;
	LineEdit_Filetypes->setEnabled(state);
}


void MainWindow::OnToggleEjectDevice (bool state)
{
	app.dm.eject_when_done = state;
}


void MainWindow::beginCopying ()
{
	if (!app.copythread_is_running)
	{
		watch_target_thread.pause();

		if (app.dm.clear_before_copying == true)
		{
			if (RadioButton_DeleteOnlyCertainFiletypes->isChecked())
				app.dm.emptyDevice(getTargetDevice(), DeviceManager::getFiletypesToDelete());
			else
				app.dm.emptyDevice(getTargetDevice());
		}
			
		// WorkerThread for copying handled in dialog.
		Dialog_Progress = new ProgressDialog(ComboBox_TargetDevice->currentIndex());
		int status = Dialog_Progress->exec();

		// FIXME: The status returned by exec() isn't what I expect, really.
		// enum QDialog::DialogCode -- QDialog::Accepted:1;QDialog::Rejected:0
		// It returns always 0 by default, by [X] but also on button click...

		if (status == 0) // Returned dialog reports no error (job wasn't aborted).
			if (app.dm.eject_when_done)
				app.dm.ejectDevice(getTargetDevice());
	
		watch_target_thread.resume();
	}
}


void MainWindow::updateTargetList ()
{
	ComboBox_TargetDevice->clear();
	Label_TargetCapacity->clear();
		
	int c = app.dm.getDeviceCount();
	
	for (int i = 0; i < c; ++i)
	{
		QString temp = temp.fromStdWString(app.dm.getVolumeName(i));
		QChar l = app.dm.getDriveletter(i);
		
		if (temp.isEmpty())
			temp = (QString)l + ":";
		else
			temp.append(" (" + (QString)l + ":)");

		ComboBox_TargetDevice->addItem(temp);
	}
}	


void MainWindow::OnSourceChanged ()
{
	checkPrerequisitesForCopying();

	if (app.source_path.isEmpty())
		Button_AddPath->setEnabled(false);
	else
		Button_AddPath->setEnabled(true);

	if (List_Sources->count() == 0)
	{
		Button_RemovePath->setEnabled(false);
		Button_ClearAll->setEnabled(false);
	}
	else
	{
		Button_ClearAll->setEnabled(true);
	}
}


void MainWindow::OnTargetChanged (int index)
{
	// The initial connection crashes due to call of clear() initiated by the same slot; http://stackoverflow.com/questions/19785530/why-does-qcomboboxremoveitem-crash (last comment).
	// Temporarily breaking the connection helps; looks like an ugly workaround, but no bad word about it here: http://qt-project.org/forums/viewthread/2964/P15
	disconnect(ComboBox_TargetDevice, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::OnTargetChanged);

	if (index != -1)
	{
		if (app.dm.getDeviceCount() == 0)
		{
			ComboBox_TargetDevice->setEnabled(false);
			Button_OpenTarget->setEnabled(false);
			updateTargetList();
		}
		else
		{
			ComboBox_TargetDevice->setEnabled(true);
			Button_OpenTarget->setEnabled(true);
			updateTargetList();
			ComboBox_TargetDevice->setCurrentIndex(index);
			emit TargetChanged(QString(app.dm.getDriveletter(getTargetDevice())) + ":\\");
		}
	}
	
	refreshTargetView();
	checkPrerequisitesForCopying();

	// ... and establish the connection again.
	connect(ComboBox_TargetDevice, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::OnTargetChanged);
}


void MainWindow::OnTargetChangeNotification (QString target)
{
	refreshTargetView();
}


void MainWindow::OnUpdateTargetDirectory (QString newtext)
{
	DeviceManager::setTargetDirectory(newtext.toStdWString());
}


void MainWindow::OnUpdateFiletypesToDelete (QString newtext)
{
	DeviceManager::setFiletypesToDelete(newtext.toStdWString());
}


void MainWindow::OnTogglePlaylistFile (bool state)
{
	app.create_playlist_file = state;
	LineEdit_PlaylistFileName->setEnabled(state);
	ComboBox_PlaylistFileFormat->setEnabled(state);
}


void MainWindow::OnUpdatePlaylistFileName (QString newtext)
{
	app.playlist_filename = newtext;
}


void MainWindow::OnUpdatePlaylistFileFormat (const QString& text)
{
	int i = ComboBox_PlaylistFileFormat->findText(text);

	if (i != -1)
	{
		app.playlist_fileformat = text;
		ComboBox_PlaylistFileFormat->setCurrentIndex(i);
	}
}


QString MainWindow::truncatePath (QString input, int width) const
{
	QFontMetrics qfm(QFont::QFont());
	return qfm.elidedText(input, Qt::ElideMiddle, width - 5); // "- 5": A buffer, to prevent that the window's width grows; maybe a more elegant way is a size policy for the widget, but haven't found the right one yet.
}


int MainWindow::getTargetDevice ()
{
	return ComboBox_TargetDevice->currentIndex();
}

void MainWindow::refreshSourceView (Playlist pl)
{
	List_Sources->clear();
	
	std::set<QString> dirs = pl.getDirectories();
	
	for (std::set<QString>::iterator j = dirs.begin(); j != dirs.end(); ++j)
	{
		List_Sources->addItem(*j);
	}

	emit SourceChanged();
}


void MainWindow::refreshTargetView ()
{
	int i = getTargetDevice();
	
	if (i != -1)
	{
		app.dm.updateFreeCapacity(i);
		Label_TargetCapacity->setText(tr("%1 MB available (of %2 MB in total).").arg(QString::number(app.dm.getFreeCapacity(i, cuMEGABYTE))).arg(QString::number(app.dm.getTotalCapacity(i, cuMEGABYTE))));
	}
	else
	{
		Label_TargetCapacity->clear();
	}
}


// Registers the device or type of device for which a window will receive notifications.
bool MainWindow::registerForDeviceNotification (void)
{
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;

	ZeroMemory (&NotificationFilter, sizeof(NotificationFilter));
	NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilter.dbcc_classguid = GUID_DEVINTERFACE_USB_DEVICE;

	hDevNotify = RegisterDeviceNotification((HWND)this->winId(), &NotificationFilter, DEVICE_NOTIFY_WINDOW_HANDLE);
	return(NULL != hDevNotify);
}


void MainWindow::checkPrerequisitesForCopying ()
{
	const std::set<QString> d = app.pl.getDirectories();

	if ((!d.empty()) && (getTargetDevice() != -1))
		Button_OK->setEnabled(true);
	else
		Button_OK->setEnabled(false);

	//TODO: If false/disabled offer some kind of info/hint to the user (status bar, pop-up, ...).
}


// Reimplemented special event handler to receive native platform events (in this case, the Windows Message Loop).
bool MainWindow::nativeEvent (const QByteArray &eventType, void* message, long* result)
{
	MSG *msg = static_cast<MSG*>(message);

	if (msg->message == WM_DEVICECHANGE)
	{
		OnDeviceChange(msg->wParam, msg->lParam);
		return true;
	}

	return false;
}


void MainWindow::OnAboutDialog ()
{
	QString title;
	title.append("<h2><a href=\"").append(QString::fromStdWString(APP_URL)).append("\">").append(QString::fromStdWString(APP_NAME)).append("</a></h2>");
	
	QString data;
	data.append("<p>Version: ").append(QString::fromStdWString(VERSION_STRING)).append("<br/>Build Time: ").append("-"/*QString::fromStdWString(BUILDTIME)*/).append("<br/>Git Commit Hash: ").append(QString::fromStdWString(GIT_COMMIT_HASH)).append("</p>");

	QString legal;
	legal.append("<p>").append(QString::fromStdWString(COPYRIGHT_NOTICE));
	legal.append("<br/>Published under the terms of the <a href=\"").append(QString::fromStdWString(LICENSE_URL)).append("\">").append(QString::fromStdWString(LICENSE_NAME)).append("</a> license").append("</p>");

	QString external;
	external.append("<p>Powered by Qt ").append(QT_VERSION_STR).append("</p>");

	QString all;
	all.append(title).append(data).append(legal).append(external);
	
	QMessageBox::about(this, tr("About"), all);
}


void MainWindow::OnOpenHelp ()
{
	QString path = QCoreApplication::applicationDirPath() + "/help/" + "help_" + app.language_code + ".html";
	QDesktopServices::openUrl(QUrl::fromLocalFile(path));
}


void MainWindow::OnLanguageChanged (QAction* action)
{
	setLanguage(action->data().toString());
}


void MainWindow::setLanguage (QString lang)
{
	if (lang == "AUTO")
	{
		app.language_mode = "AUTO";
		QLocale loc;
		app.language_code = loc.bcp47Name(); // Note: Qt 5 currently returns for example "de", but could be "de_DE".
		app.language_code = app.language_code.toUpper();
		lang_AUTO->setChecked(true);
		QString f = "lang_" + app.language_code + ".qm";
		app.switchTranslator(f);
		updateTranslation();
	}

	if (lang == "EN")
	{
		app.language_mode = app.language_code = "EN";
		lang_EN->setChecked(true);
		QString f = "lang_" + app.language_code + ".qm";
		app.switchTranslator(f);
		updateTranslation();
	}

	if (lang == "DE")
	{
		app.language_mode = app.language_code = "DE";
		lang_DE->setChecked(true);
		QString f = "lang_" + app.language_code + ".qm";
		app.switchTranslator(f);
		updateTranslation();
	}
}


void MainWindow::updateTranslation ()
{
	// Text in menu bar.
	Menu_Language->setTitle(tr("Language"));
	lang_AUTO->setText(tr("Automatic"));
	lang_EN->setText(tr("EN (English)"));
	lang_DE->setText(tr("DE (German)"));

	Menu_Help->setTitle(tr("Help"));
	Action_AboutDialog->setText(tr("About"));
	Action_OpenHelp->setText(tr("Open help in Browser"));

	// Text in 'Source' area.
	Group_Source->setTitle(tr("Source"));
	Button_ChangePath->setText(tr("Change..."));
	CheckBox_IncSubDir->setText(tr("Include sub-directories"));
	Button_AddPath->setText(tr("Add path"));
	Label_ListHeader->setText(tr("Directories:"));
	Button_RemovePath->setText(tr("Remove path"));
	Button_ClearAll->setText(tr("Clear"));
	Button_OpenSource->setText(tr("Open"));
	Button_Filter->setText(tr("Filter..."));
	CheckBox_PlaylistFile->setText(tr("Generate Playlist File"));

	// Text in 'Target' area.
	Group_Target->setTitle(tr("Target"));
	Button_OpenTarget->setText(tr("Open"));
	refreshTargetView();
	Label_CapacityLimit->setText(tr("Use max. % of free capacity:"));
	CheckBox_UseDir->setText(tr("Copy files into this directory:"));
	CheckBox_EmptyDeviceBeforeCopying->setText(tr("Empty device before copying"));
	RadioButton_DeleteAllFiles->setText(tr("Delete everything under this path:"));
	RadioButton_DeleteAllFiles->setToolTip(tr("Deletes all files on the selected target drive.\nIf a target directory is given, this will act only within that directory (if it already exists).\nCAUTION: This will delete the content without asking for confirmation and without putting the files in the recycle bin!"));
	RadioButton_DeleteOnlyCertainFiletypes->setText(tr("Delete only these filetypes:"));
	RadioButton_DeleteOnlyCertainFiletypes->setToolTip(tr("Deletes only files of given type(s).\nEnter the plain extension (example: TXT for *.TXT), seperate multiple values with a comma (example: TXT,MP3,MKV).\nActs non-recursively on the directory.\nCAUTION: This will delete the content without asking for confirmation and without putting the files in the recycle bin!"));
	CheckBox_EjectDevice->setText(tr("Eject device when done"));

	// Misc.
	Button_OK->setText(tr("Begin"));
	String_OnChangeSource = tr("Select source directory");
}


void Widget::closeEvent (QCloseEvent* event)
{
	// Moved from Application::~Application() here because the static data member target_directory
	// isn't written to the settings file when invoked by this destructor (too late?).
	app.saveSettings();
}

#include "moc_MainWindow.cpp"