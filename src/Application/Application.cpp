// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include <shlobj.h>		    // SHGetFolderPath()
#include <iostream>

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>
#include <QtWidgets/QMessageBox>
#include <QtCore/QSettings>

#include "Application.h"
#include "version.h"

Application::Application ()
{
	copy_job_progress = 0;
	loadSettings(); // Load settings from *.ini file into structure.
}

Application::~Application ()
{
	// saveSettings(); // The static data member target_directory isn't written to the settings file when invoked by this destructor (too late?).
}

void Application::loadSettings ()
{
	// The default values.
	supported_playlistfile_formats = new QList<QString>;
	supported_playlistfile_formats->append("m3u");
	supported_playlistfile_formats->append("pls");
	source_path.clear();
	include_subdirectory = true;
	create_playlist_file = true;
	dm.clear_before_copying = false;
	dm.eject_when_done = false;
	dm.max_percent = 100;
	dm.use_directory = false;
	DeviceManager::setTargetDirectory(L"");
	language_mode = "EN";

	// Get location of user's application data directory, search for settings file there.
	// If there is a configuration file, override the default values.
	// CSIDL_APPDATA is '%APPDATA%\Roaming' on the Windows platform.
	wchar_t user_appdata_path [MAX_PATH];

	if (SHGetSpecialFolderPath(NULL, user_appdata_path, CSIDL_APPDATA, FALSE))
	{
		QString settings_file_path = QString::fromWCharArray(user_appdata_path) + "\\" + QString::fromStdWString(APP_NAME) + "\\settings.ini";
		QSettings config(settings_file_path, QSettings::IniFormat);
		config.setIniCodec("UTF-8");

		if (config.contains("Language"))
		{
			QString lang = config.value("Language").toString();

			if (0 == lang.compare("AUTO", Qt::CaseInsensitive))
			{
				language_mode = "AUTO";
				goto Done;
			}

			if (0 == lang.compare("EN", Qt::CaseInsensitive))
			{
				language_mode = "EN";
				goto Done;
			}

			if (0 == lang.compare("DE", Qt::CaseInsensitive))
			{
				language_mode = "DE";
				goto Done;
			}

			Done: ; // Empty label/null statement, just to get out of here.
		}

		if (config.contains("Source/DefaultDirectory"))
			source_path = config.value("Source/DefaultDirectory").toString();

		if (config.contains("Source/IncludeSubDir"))
			config.value("Source/IncludeSubDir") == "1" ? include_subdirectory = true : include_subdirectory = false;

		if (config.contains("Source/CreatePlaylistFile"))
			config.value("Source/CreatePlaylistFile") == "1" ? create_playlist_file = true : create_playlist_file = false;

		if (config.contains("Source/PlaylistFileName"))
		{
			playlist_filename = config.value("Source/PlaylistFileName").toString();
			if (playlist_filename.isEmpty())
				playlist_filename = "playlist";
		}
		else
		{
			playlist_filename = "playlist";
		}

		if (config.contains("Source/PlaylistFileFormat"))
		{
			playlist_fileformat = config.value("Source/PlaylistFileFormat").toString();
			if (playlist_fileformat.isEmpty())
				playlist_fileformat = "m3u";
		}

		if (config.contains("Device/ClearDevice"))
			config.value("Device/ClearDevice") == "1" ? dm.clear_before_copying = true : dm.clear_before_copying = false;

		if (config.contains("Device/ClearDeviceMode"))
		{
			int mode = config.value("Device/ClearDeviceMode").toInt();

			switch (mode)
			{
				case ALL_FILES:
					dm.clear_before_copying_all = true;
					dm.clear_before_copying_filetypes = false;
					break;
				
				case CERTAIN_FILETYPES:
					dm.clear_before_copying_all = false;
					dm.clear_before_copying_filetypes = true;
					break;
				
				default:
					dm.clear_before_copying_all = true;
					dm.clear_before_copying_filetypes = false;
			}
		}

		if (config.contains("Device/EjectDevice"))
			config.value("Device/EjectDevice") == "1" ? dm.eject_when_done = true : dm.eject_when_done = false;
				
		if (config.contains("Device/UseMaxPercent"))
			dm.max_percent = config.value("Device/UseMaxPercent").toInt();

		if (config.contains("Device/FiletypesToDelete"))
		{
			QString qs = config.value("Device/FiletypesToDelete").toString();
			DeviceManager::setFiletypesToDelete(qs.toStdWString());
		}

		if (config.contains("Device/UseDirectory"))
			config.value("Device/UseDirectory") == "1" ? dm.use_directory = true : dm.use_directory = false;
				
		if (config.contains("Device/DirectoryName"))
		{
			QString qs = config.value("Device/DirectoryName").toString();
			DeviceManager::setTargetDirectory(qs.toStdWString());
		}
	}
}

void Application::saveSettings ()
{
	wchar_t user_appdata_path [MAX_PATH];
	QString settings_file_path;
	
	if (SHGetSpecialFolderPath(NULL, user_appdata_path, CSIDL_APPDATA, FALSE))
	{
		QString settings_file_path = QString::fromWCharArray(user_appdata_path) + "\\" + QString::fromStdWString(APP_NAME) + "\\settings.ini";

		QSettings config(settings_file_path, QSettings::IniFormat);
		config.setIniCodec("UTF-8");

		config.setValue("Language", language_mode);

		config.beginGroup("Source");
			config.setValue("DefaultDirectory", QDir::toNativeSeparators(source_path));
			config.setValue("IncludeSubDir", (include_subdirectory == true ? "1" : "0"));
			config.setValue("CreatePlaylistFile", (create_playlist_file == true ? "1" : "0"));
			config.setValue("PlaylistFileName", playlist_filename);
			config.setValue("PlaylistFileFormat", playlist_fileformat);
		config.endGroup();

		config.beginGroup("Device");
			config.setValue("ClearDevice", (dm.clear_before_copying == true ? "1" : "0"));
			config.setValue("ClearDeviceMode", (dm.clear_before_copying_all == true ? ALL_FILES : CERTAIN_FILETYPES));
			config.setValue("EjectDevice", (dm.eject_when_done == true ? "1" : "0"));
			config.setValue("UseMaxPercent", dm.max_percent);
			config.setValue("FiletypesToDelete", QString::fromStdWString(DeviceManager::getFiletypesToDeleteAsText()));
			config.setValue("UseDirectory", (dm.use_directory == true ? "1" : "0"));
			config.setValue("DirectoryName", QString::fromStdWString(DeviceManager::getTargetDirectory()));
		config.endGroup();
	}
}


void Application::switchTranslator (const QString& filename)
{
	QCoreApplication::removeTranslator(&translator);

	if(translator.load(filename, "lang"))
		QCoreApplication::installTranslator(&translator);
}
