// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <string>
#include <vector>
#include <set>

#include <QtCore/QObject>
#include <QtCore/QFile>

#include "../DevicesLib/DeviceManager.h"

class Playlist : public QObject
{
	Q_OBJECT

	public:
		Playlist ();
		Playlist (const Playlist& original_object);
		~Playlist ();
		
		Playlist& operator= (const Playlist& right);
		
		int  addDirectory    (const QString path, bool include_subdirectories = true);
		void removeDirectory (const QString dir);
		
		void copyFile (Playlist* target, int i);
		void shuffle ();
		
		void clear ();
		void unmark ();
		
		int getNumberOfEntries () const;
		
		const QString getDirectory (int index) const;
		const QString getFilename  (int index) const;
		
		const std::set<QString> getDirectories () const;
		
		void copy (DeviceManager dm, int index); // rename?

	signals:
		void CopyingStatus (int percent, int filecount, QString filename);
		void CopyingAborted (int errorcode);
		void CopyingAbortedByUser ();
		void CopyingDone (int filecount);

	private:		
		
		struct entry
		{
			QString                directory;
			QString                filename;
			unsigned long long int filesize;
			bool                   marked;
		};
		
		std::vector<entry*> pl;
		std::set<QString> list_of_directories; // This is used for a permanent storage (set = no duplicates), so that the GUI can rebuild its list.
		
		int       number_of_files_on_playlist;
		long long playlist_size;               // in bytes.
	    
		void removeFile           (std::vector<entry*>::iterator i);
		void removeFile           (std::vector<entry*>::reverse_iterator i);
		int  generateRandomNumber (int n);
		
		void preparePlaylistFile (QFile* file, QString format);
		void updatePlaylistFile  (QFile* file, QString format, QString text, int index);
		void finishPlaylistFile  (QFile* file, QString format, int count);
};

#endif
