// Copyright 2005-2014 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.

#ifndef FILTER_H
#define FILTER_H

#include <map>
#include <string>

#include "Playlist.h" // o. nur 'exter'-decl.?

/*

wie man es von Firewalls gew�hnt ist: greift ein accept oder deny, dann ist die Datei entweder drin oder
nicht, greift keine Regel, ist es akzeptiert, sofern man als (sinnvollerweise) letzte Regel keinen
catch-all f�r deny setzt ("deny " ohne String).

Hier ist default "accept", andernfalls w�re ein leeres "deny " als letzter Eintrag n�tig.
"Add filter" �ffnet einen kleinen Dialog der einen Filterstring abfragt, sowie �ber zwei
Radiobuttons nach accept oder deny fragt. "Remove filter" entfernt den markierten Filter,
Doppelklick auf einen Eintrag in der Listbox entfernt diesen ebenfalls.

Die Filter werden (ebenso wie die aktuelle Playlist, die Fensterposition, Zustand der
Checkbox und das ausgew�hlte Laufwerk) in der .ini Datei abgelegt, sind also jedesmal
wie man sie verlassen hat.

*/

class Filter
{
	public:
		Playlist* apply (Playlist* source); // rename? "apply"?
		
		void addRule (std::string subject, bool accept);
		void addRule (char* subject, bool accept);
		
		void removeRule (std::string subject);
		void removeRule (char* subject);
		void clear      ();
		
		void acceptAll ();
		void rejectAll ();
		
		bool queryAcceptAll () const;
		
		void updateList (HWND dlg);
		
		bool isAccepted (std::string);
		bool isEmpty () const;
	
	private:
		std::map<std::string, bool> rules;
		bool accept_all;
};

#endif
