// ☺
// Copyright © 2014-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include <QtWidgets/QDialog>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

#include "Application.h"
#include "ProgressDialog.h"
#include "CopyThread.h"

extern Application app;

ProgressDialog::ProgressDialog (int target_index)
{
	copythread = new CopyThread(target_index);
		
	updateTranslation();
	
	setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);
	setWindowTitle(text_windowtitle);
	
	List_Files = new QListWidget();

	ProgressBar_Progress = new QProgressBar;
	ProgressBar_Progress->setRange(0, 100); // 0-100%
	
	Label_Summary = new QLabel();

	Button_AbortClose = new QPushButton(text_abort);
	Button_AbortClose->setFixedSize(Button_AbortClose->sizeHint());

	Layout = new QVBoxLayout;
	Layout->addWidget(List_Files);
	Layout->addWidget(ProgressBar_Progress);
	Layout->addWidget(Label_Summary, 0);
	Layout->addWidget(Button_AbortClose, 0, Qt::AlignCenter);
	this->setLayout(Layout);

	connect(Button_AbortClose, SIGNAL(clicked(bool)), this, SLOT(OnControlButton()));
	connect(&app.pl, SIGNAL(CopyingStatus (int, int, QString)), this, SLOT(OnCopyingStatus (int, int, QString)));
	connect(&app.pl, SIGNAL(CopyingAborted (int)), this, SLOT(OnCopyingAborted (int)));
	connect(&app.pl, SIGNAL(CopyingDone (int)), this, SLOT(OnCopyingDone (int)));
	connect(&app.pl, SIGNAL(CopyingAbortedByUser ()), this, SLOT(OnCopyingAbortedByUser ()));
	connect(this, SIGNAL(rejected()), this, SLOT(OnRejected()));
	
	copythread->start();
}

void ProgressDialog::OnControlButton ()
{
	if (app.copythread_is_running)
	{
		app.mutex.lock();
			app.stop_copy_thread = true;
			app.copythread.wait(&app.mutex, 3000); // 3000ms = 3sec
		app.mutex.unlock();

		app.copy_job_progress = 0;
		app.stop_copy_thread = false; // Clear flag, in case we want to restart the copying.
	}
	else
	{
		this->close();
	}
}

void ProgressDialog::OnCopyingStatus (int percent, int filecount, QString filename)
{
	List_Files->addItem(filename);
	List_Files->scrollToBottom();
	
	Label_Summary->setText(tr("%n file(s) copied...", "", filecount));
	
	ProgressBar_Progress->setValue(percent);
	app.copy_job_progress = percent;
}

void ProgressDialog::OnCopyingAborted (int errorcode)
{
	Label_Summary->setText(tr("Copying aborted! Error code: 0x%1").arg(QString::number(errorcode, 16)));
	Button_AbortClose->setText(text_close);
	app.copy_job_progress = 0;
	progress_status = 1; // The job was aborted.
}

void ProgressDialog::OnCopyingDone (int filecount)
{
	Label_Summary->setText(tr("Done! %n file(s) copied.", "", filecount));
	ProgressBar_Progress->setValue(100); // [TODO] Hm, just because we're done doesn't mean we have accomplished 100%.
	Button_AbortClose->setText(text_close);

/*
	if (::IsIconic(h))
	{
		FLASHWINFO fwi;
		fwi.cbSize    = sizeof(fwi);
		fwi.hwnd      = h;
		fwi.dwFlags   = 0x00000003;
		fwi.uCount    = 1;
		fwi.dwTimeout = 0;
		FlashWindowEx(&fwi);
	}
*/
	app.saveSettings();            // FIXME: Lock.
	app.copy_job_progress = 0;

	progress_status = 0; // Everything's fine.
}

void ProgressDialog::OnCopyingAbortedByUser ()
{
	Label_Summary->setText(tr("Copying aborted by user!"));
	Button_AbortClose->setText(text_close);
	app.copy_job_progress = 0;
	progress_status = 1; // The job was aborted.
}

void ProgressDialog::updateTranslation ()
{
	text_windowtitle = tr("Progress");
	text_abort       = tr("Abort");
	text_close       = tr("Close");
}

// For example when dismissed via 'Escape' key.
// [FIXME] Resembles mostly the code from ProgressDialog::OnControlButton() -- but invoking
//         that method here doesn't have the same effect.
void ProgressDialog::OnRejected ()
{
	if (app.copythread_is_running)
	{
		app.mutex.lock();
			app.stop_copy_thread = true;
			app.copythread.wait(&app.mutex, 3000); // 3000ms = 3sec
		app.mutex.unlock();

		app.copy_job_progress = 0;
		app.stop_copy_thread = false; // Clear flag, in case we want to restart the copying.
	}

	this->close();
}
