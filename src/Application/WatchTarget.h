/*
┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                             RandFill                                             │
└──────────────────────────────────────────────────────────────────────────────────────────────────┘
Copyright © 2015, 2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see accompanying file 'License.txt' for details).
*/

#ifndef WATCHTARGET_H
#define WATCHTARGET_H

#include <QtCore/QString>
#include <QtCore/QThread>

class WatchTargetThread : public QThread
{
	Q_OBJECT

	public:
		void pause ();
		void resume ();

	signals:
		void pauseThread ();
		void resumeThread ();
};

class WatchTargetWorker : public QObject
{
	Q_OBJECT

	public:
		WatchTargetWorker ();
		~WatchTargetWorker ();

	signals:
		void ChangeNotification (QString target);
		void finished ();

	public: // Qt slot functions.
		void work ();
		void pause ();
		void resume ();
		void OnTargetChanged (QString new_target);

	private:
		volatile bool thread_paused;
		volatile bool target_changed;
		volatile DWORD wait_status; 
		volatile HANDLE handle;
		QString target;
};

#endif