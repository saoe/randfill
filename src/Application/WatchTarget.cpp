/*
┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                             RandFill                                             │
└──────────────────────────────────────────────────────────────────────────────────────────────────┘
Copyright © 2015, 2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see accompanying file 'License.txt' for details).

The watcher thread classes that watches the target device and reports any changes to it, like
addition or deletion of files and directories. (Contains also the worker helper class for this.)
*/

#include <windows.h>
#include "WatchTarget.h"

void WatchTargetThread::pause ()
{
	emit pauseThread();
}

void WatchTargetThread::resume ()
{
	emit resumeThread();
}

// ------------------------------------------------------------------------------

WatchTargetWorker::WatchTargetWorker () : thread_paused(false), target_changed(false)
{
}

WatchTargetWorker::~WatchTargetWorker ()
{
	FindCloseChangeNotification(handle);
}

void WatchTargetWorker::work ()
{
	start:
	
	handle = FindFirstChangeNotification( 
		reinterpret_cast<LPCWSTR>(target.utf16()), // Directory to watch.
		TRUE,                                      // Watch sub-subdirectories.
		FILE_NOTIFY_CHANGE_FILE_NAME  | FILE_NOTIFY_CHANGE_SIZE);

	if (handle == INVALID_HANDLE_VALUE) 
		emit finished();
	
	while (true)
	{
		if (!target.isEmpty())
		{
			if (target_changed)
			{	
				target_changed = false;
				FindCloseChangeNotification(handle);
				goto start;
			}
			else
			{
				if (!thread_paused)
				{
					wait_status = WaitForSingleObject(handle, 1000); // Wait 1s (1000ms), because INFINITE would block after a change of the target device (path). 

					switch (wait_status) 
					{ 
						case WAIT_OBJECT_0: 
							emit ChangeNotification(target);
					
							if (FindNextChangeNotification(handle) == FALSE)
								emit finished(); 
							break; 
					}
				}
			}
		}
	}
}

void WatchTargetWorker::pause ()
{
	thread_paused = true;
}

void WatchTargetWorker::resume ()
{
	thread_paused = false;
}

void WatchTargetWorker::OnTargetChanged (QString new_target)
{
	target_changed = true;
	target = new_target;
}
