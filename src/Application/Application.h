// ☺
// Copyright © 2005-2017 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#ifndef APPLICATION_H
#define APPLICATION_H

#include <windows.h>
#include <QtCore/QCoreApplication>
#include <QtCore/QString>
#include <QtCore/QWaitCondition>
#include <QtCore/QTranslator>
#include <QtCore/QMutex>

#include "Playlist.h"
#include "../DevicesLib/Device.h"
#include "../DevicesLib/DeviceManager.h"

class Application
{
	Q_DECLARE_TR_FUNCTIONS(Application) // To enable translation via tr() in a class without a Q_OBJECT.

	public:
		 Application ();
		~Application ();

		QString source_path;       // The path to the current/last source directory.
		bool include_subdirectory; // Determines whether sub-directories of PATH should be included (true) or not (false).
		bool create_playlist_file;
		QString playlist_filename;
		QList<QString>* supported_playlistfile_formats;
		QString playlist_fileformat;
		int copy_job_progress;
		QString language_mode;
		QString language_code;
		QTranslator translator;

		DeviceManager dm;
		Playlist      pl;

		void loadSettings ();
		void saveSettings ();
		void switchTranslator (const QString& filename);

		QWaitCondition copythread;
		QMutex mutex;
		volatile bool stop_copy_thread;
		volatile bool copythread_is_running;

	private:
		enum ClearDeviceMode {ALL_FILES, CERTAIN_FILETYPES};
};

#endif
