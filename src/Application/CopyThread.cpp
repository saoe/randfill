// ☺
// Copyright © 2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include "Application.h"
#include "Playlist.h"
#include "CopyThread.h"

extern Application app;

CopyThread::CopyThread (int target_index)
{
	index = target_index;
	
	mutex.lock();
		app.copythread_is_running = false;
	mutex.unlock();
}

void CopyThread::run ()
{
	mutex.lock();
		app.copythread_is_running = true;
	mutex.unlock();

	app.pl.copy(app.dm, index);
	
	mutex.lock();
		app.copythread_is_running = false;
	mutex.unlock();
}
