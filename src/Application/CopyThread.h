// Copyright 2015-2017 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.

#ifndef COPYTHREAD_H
#define COPYTHREAD_H

#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtCore/QWaitCondition>
#include <QtCore/QMutex>

// The worker-thread-procedure; calls the copy-routine.

class CopyThread : public QThread
{
	Q_OBJECT
	
	public:
		CopyThread (int target_index);

	protected:
		void run ();

	private:
		QMutex  mutex;
		int index; // Index of the target device.
};

#endif