/*
┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                             RandFill                                             │
└──────────────────────────────────────────────────────────────────────────────────────────────────┘
Copyright © 2005-2015, 2019 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license (see accompanying file 'License.txt' for details).
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "WatchTarget.h"

class QLabel;
class QGridLayout;
class QGroupBox;
class QPushButton;
class QCheckBox;
class QListWidget;
class QComboBox;
class QSlider;
class QSpinBox;
class QButtonGroup;
class QRadioButton;
class QLineEdit;
class QSizePolicy;
class QSpacerItem;
class QHBoxLayout;
class QVBoxLayout;
class QCloseEvent;
class QActionGroup;

class Playlist;
class ProgressDialog;

class Widget : public QWidget
{
	protected:
		void closeEvent (QCloseEvent* event);
};


class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		MainWindow();
		~MainWindow();

		bool nativeEvent (const QByteArray &eventType, void* message, long* result);

		// Qt slot functions.
		void OnDeviceChange (WPARAM wParam, LPARAM lParam);
		void OnUseCapacity (int value);
		void OnChangeSource ();
		void OnToggleIncsubdir (bool state);
		void OnTogglePlaylistFile (bool state);
		void addSource ();
		void removeSource ();
		void clearSource ();
		void openSourceInExplorer ();
		void openTargetInExplorer ();
		void OnToggleUseDirectory (bool state);
		void OnToggleClearDevice (bool state);
		void OnToggleDeleteAllFiles (bool state);
		void OnToggleDeleteOnlyCertainFiletypes (bool state);
		void OnToggleEjectDevice (bool state);
		void OnChangeSourceSelection();
		void beginCopying ();
		void OnSourceChanged ();
		void OnTargetChanged (int index);
		void OnTargetChangeNotification (QString target);
		void OnUpdateTargetDirectory (QString newtext);
		void OnUpdateFiletypesToDelete (QString newtext);
		void OnUpdatePlaylistFileName (QString newtext);
		void OnUpdatePlaylistFileFormat (const QString& text);
		void OnAboutDialog ();
		void OnOpenHelp ();
		void OnLanguageChanged (QAction* action);
		void updateLabelWhenTargetChanged ();
		void updateTargetList ();

	signals:
		void SourceChanged ();
		void TargetChanged (int value);
		void TargetChanged (QString new_target_path);

	private:
		Widget* MainWidget;

		// GUI: Menubar:
		QMenu*   Menu_Language;
		QMenu*   Menu_Help;
		QAction* Action_AboutDialog;
		QAction* Action_OpenHelp;
		QAction* lang_AUTO;
		QAction* lang_EN;
		QAction* lang_DE; 
		
		// GUI: "Source" group:
		QGroupBox*    Group_Source;
		QLabel*       Label_SourcePath;
		QPushButton*  Button_ChangePath;
		QCheckBox*    CheckBox_IncSubDir;
		QPushButton*  Button_AddPath;
		QLabel*       Label_ListHeader;
		QListWidget*  List_Sources;
		QPushButton*  Button_RemovePath;
		QPushButton*  Button_ClearAll;
		QPushButton*  Button_OpenSource;
		QPushButton*  Button_Filter;
		QHBoxLayout*  Layout_SourceGroupPlaylistFile;
		QVBoxLayout*  Layout_SourceGroup1;
		QGridLayout*  GridLayout_Source;
		QCheckBox*    CheckBox_PlaylistFile;
		QLineEdit*    LineEdit_PlaylistFileName;
		QComboBox*    ComboBox_PlaylistFileFormat;

		// GUI: "Target" group:
		QGroupBox*    Group_Target;
		QHBoxLayout*  Layout_TargetGroup3;
		QComboBox*    ComboBox_TargetDevice;
		QLabel*       Label_TargetCapacity;
		QPushButton*  Button_OpenTarget;
		QHBoxLayout*  Layout_TargetGroup1;
		QLabel*       Label_CapacityLimit;
		QSpinBox*     SpinBox_CapacityLimit;
		QSlider*      Slider_CapacityControl;
		QHBoxLayout*  Layout_TargetGroup2;
		QCheckBox*    CheckBox_UseDir;
		QLineEdit*    LineEdit_DirName;
		QCheckBox*    CheckBox_EmptyDeviceBeforeCopying;
		QButtonGroup* ButtonGroup_EmptyDeviceBeforeCopying;
		QRadioButton* RadioButton_DeleteAllFiles;
		QLabel*       Label_DeleteAllFiles;
		QRadioButton* RadioButton_DeleteOnlyCertainFiletypes;
		QLineEdit*    LineEdit_Filetypes;
		QCheckBox*    CheckBox_EjectDevice;
		QGridLayout*  GridLayout_Target;
		QVBoxLayout*  Layout_TargetGroup0;
		QPushButton*  Button_OK;

		// GUI: The master layout:
		QVBoxLayout* MainLayout;

		// GUI: Progress dialog:
		ProgressDialog* Dialog_Progress;

		// Other variables:
		HDEVNOTIFY        hDevNotify;
		QString           String_OnChangeSource;
		WatchTargetThread watch_target_thread;
		WatchTargetWorker watch_target_worker;

		// Other methods:
		QString truncatePath (QString input, int width) const;
		int     getTargetDevice ();
		void    refreshSourceView (Playlist pl);
		void    refreshTargetView ();
		bool    registerForDeviceNotification (void);
		void    checkPrerequisitesForCopying ();
		void    setLanguage (QString lang);
		void    updateTranslation ();
};

#endif
