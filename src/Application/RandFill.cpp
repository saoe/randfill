// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include <windows.h>
#include <QtWidgets/QApplication>

#include "MainWindow.h"
#include "Application.h"

Application app;

int main (int argc, char* argv[])
{
	QApplication qtapp(argc, argv);
	MainWindow mainwindow;
	return qtapp.exec();
}
