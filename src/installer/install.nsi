# ☺
# Nullsoft Install System (NSIS) script for the creation of the installer/setup file.
# 
# Copyright © 2013-2018 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license. For details, see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>.
#
# Severals definitions (/D...=) are expected to be given as an commandline argument to makensis.exe:
# NAME              : The project's name.
# EXE               : Name of the main program executable, e.g. NiftyOddity.exe.
# INSTALLER_FILENAME: Name of the setup file.
# VERSION           : Version of the program.
# BUILDPATH         : The full path to the directory that contains the files which will go into the setup (the source files).
# OUTPATH           : The full path to the directory that will get the setup.exe (the target directory).
# ARCH              : The processor architecture, e.g. x86, x64 or amd64.
# VCREDIST          : The filename of the Microsoft Visual C++ Redistributable Package.
# ------------------------------------------------------------------------------

Name "${NAME}"
!define PROJECTNAME "${NAME}"
!define AUTHOR "Sascha Offe"

# ${BUILDPATH}-setup must exist!
# Tried to create it here, but CMake provides forward slashes and NSIS uses Windows' mkdir, which only accepts backward slashes.
OutFile "${OUTPATH}\${INSTALLER_FILENAME}"

# Set default value for $INSTDIR, depending on the processor architecture (64-bit/x64 or 32-bit/x86).
!if ${ARCH} == "x64"
	InstallDir $PROGRAMFILES64\${PROJECTNAME}
!else
	InstallDir $PROGRAMFILES\${PROJECTNAME}
!endif

!define UNINST_REGKEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PROJECTNAME}"

BrandingText " " # Don't show the "Nullsoft Install System vX.XX" note.

# Multi User support <http://nsis.sourceforge.net/Docs/MultiUser/Readme.html>
!define MULTIUSER_EXECUTIONLEVEL Highest
!define MULTIUSER_INSTALLMODE_COMMANDLINE
!define MULTIUSER_MUI
!include MultiUser.nsh

# Support for the 'Modern User Interface 2'.
!include "MUI2.nsh"
!define MUI_ICON "..\res\randfill.ico"
!define MUI_WELCOMEPAGE_TITLE "$(WP_TITLE)"

/*
!define MULTIUSER_INSTALLMODEPAGE_TEXT_TOP "Text to display on the top of the page."
!define MULTIUSER_INSTALLMODEPAGE_TEXT_ALLUSERS "Text to display on the combo button for a per-machine installation.
!define MULTIUSER_INSTALLMODEPAGE_TEXT_CURRENTUSER "Text to display on the combo button for a per-user installation."
*/

# Remember the installer language. Put before the pages. Don't forget to remove it in the uninstaller again!
!define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
!define MUI_LANGDLL_REGISTRY_KEY "Software\${PROJECTNAME}" 
!define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"

# Pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "..\..\License.txt"
!insertmacro MULTIUSER_PAGE_INSTALLMODE # Ask user: per-machine or per-user installation?
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

# Language support.
!insertmacro MUI_LANGUAGE "English" # First language is the default.
!insertmacro MUI_LANGUAGE "German"

# Language strings.
LangString WP_TITLE ${LANG_ENGLISH} "${PROJECTNAME} ${VERSION} Setup Wizard"
LangString WP_TITLE ${LANG_GERMAN}  "${PROJECTNAME} ${VERSION} Installationsassistent"

# ------------------------------------------------------------------------------
# Installation, components, uninstallation.

Section
	SetOutPath $INSTDIR
	File /r ${BUILDPATH}\*.*
	
	WriteUninstaller $INSTDIR\uninstall.exe
	
	# Create entry for "Add/Remove Programs" ("Programs and Features"); with multi-user support.
	WriteRegStr SHELL_CONTEXT "${UNINST_REGKEY}" "DisplayName" "${PROJECTNAME}"
	WriteRegStr SHELL_CONTEXT "${UNINST_REGKEY}" "Publisher" "${AUTHOR}"
	WriteRegStr SHELL_CONTEXT "${UNINST_REGKEY}" "DisplayVersion" "${VERSION}"
	WriteRegStr SHELL_CONTEXT "${UNINST_REGKEY}" "DisplayIcon" "$INSTDIR\${EXE}"
	WriteRegStr SHELL_CONTEXT "${UNINST_REGKEY}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\" /$MultiUser.InstallMode"
	WriteRegStr SHELL_CONTEXT "${UNINST_REGKEY}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /$MultiUser.InstallMode /S"
	
	# Run the setup for the Microsoft Visual C++ Redistributable Package.
	ExecWait '"$INSTDIR\${VCREDIST}" /passive /norestart'
SectionEnd

Section "Start menu shortcuts"
	CreateDirectory "$SMPROGRAMS\${PROJECTNAME}"
	CreateShortCut "$SMPROGRAMS\${PROJECTNAME}\RandFill.lnk" "$INSTDIR\${EXE}"
	CreateShortCut "$SMPROGRAMS\${PROJECTNAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe"
SectionEnd

Section "Uninstall"
	# Not using RMDir /r for reason given in <http://nsis.sourceforge.net/Reference/RMDir>:
	# "Though it is unlikely, the user might select to install to the Program Files folder and so this command will wipe out the entire Program Files folder."
	Delete $INSTDIR\help\*.*
	Delete $INSTDIR\lang\*.*
	Delete $INSTDIR\platforms\*.*
	Delete $INSTDIR\*.*
	RMDir $INSTDIR\help
	RMDir $INSTDIR\lang
	RMDir $INSTDIR\platforms
	RMDir $INSTDIR
	
	# Getting rid of the shortcuts.
	Delete "$SMPROGRAMS\${PROJECTNAME}\*.*"
	RMDir "$SMPROGRAMS\${PROJECTNAME}"
	
	# Remove entry for "Add/Remove Programs" ("Programs and Features") again.
	DeleteRegKey SHELL_CONTEXT "${UNINST_REGKEY}"
	
	# Remove other entries from registry again.
	DeleteRegKey /ifempty HKCU "Software\${PROJECTNAME}"
SectionEnd

# ------------------------------------------------------------------------------
# Functions

Function .onInit
	!insertmacro MULTIUSER_INIT      # Check if the user has the neccessary rights.
	!insertmacro MUI_LANGDLL_DISPLAY # Display language selection dialog.
FunctionEnd

Function un.onInit
	!insertmacro MULTIUSER_UNINIT  # Check if the user has the neccessary rights.
	!insertmacro MUI_UNGETLANGUAGE # Get the stored language preference for the uninstaller.
FunctionEnd
