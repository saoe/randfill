# Write data to the version information file (LatestVersion.txt).
# This file will later be uploaded, together with the installer file, in the download folder on a server
# and the auto-update mechanism of the client can use it for check and download purposes.

# Compute a cryptographic hash value of the content of the installer file.
if (EXISTS ${INSTALLER_FILE})
	file(SHA256 ${INSTALLER_FILE} HASH_VALUE)
else ()
	message (STATUS "File ${INSTALLER_FILE} does not exist.")
endif ()

# Substitute the placeholders in the template file with the actual values and write it to a different file (LatestVersion.txt.in -> LatestVersion.txt).
configure_file (${IN} ${OUT} @ONLY)