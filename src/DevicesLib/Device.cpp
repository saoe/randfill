// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include <cstdlib>
#include <stdio.h>
#include <string>
#include <fstream>

#include <windows.h>
#include <shellapi.h>
#include <setupapi.h>
#include <cfgmgr32.h>
#include <tchar.h>

#include "Device.h"
#include "DeviceManager.h"

//std::wofstream logfile("log.txt", std::ios_base::trunc);

using std::wstring;
using std::vector;

// -------------------------------------------------------------------------------------------------
// Deletes all files of a certain type on a device.
// For details, see MSDN's documentation of SHFILEOPSTRUCT member 'pFrom'.

void Device::empty (vector<wstring> input) const
{
	wstring basepath, output;
	
	if (DeviceManager::isTargetDirectoryEmpty())
		basepath = wstring(1, this->drive_letter) + _T(":\\*");
	else
		basepath = wstring(1, this->drive_letter) + _T(":\\") + DeviceManager::getTargetDirectory() + _T("\\*");
	
	if (input.empty())
	{
		output = basepath + wstring(1, _T('\0')); // The default is to delete all files.
	}
	else
	{
		// Delete only files of a certain type, listed in the supplied vector.
		// Acts only on the top-level directory (i.e. non-recursive).
		wstring temp_str;
		vector<wstring> temp_vec = DeviceManager::getFiletypesToDelete();

		for (vector<wstring>::const_iterator pos = temp_vec.cbegin(); pos != temp_vec.cend(); ++pos)
		{
			temp_str = basepath + _T(".") + (*pos) + wstring(1, _T('\0'));
			output.append(temp_str);
		}
	}

	SHFILEOPSTRUCT sfos;	
	sfos.hwnd   = NULL;
	sfos.wFunc  = FO_DELETE;
	sfos.fFlags = FOF_SILENT | FOF_NOCONFIRMATION; // Deletes silently and recursively files and (non-empty) folders. Add FOF_NORECURSION | FOF_FILESONLY for top-level-only/files-only.
	sfos.pFrom  = output.c_str();
	sfos.pTo    = NULL;
	
	SHFileOperation(&sfos);
}


// -------------------------------------------------------------------------------------------------
// Returns the free capacity of the device (in "units").

int Device::getFreeCapacity (capacity_unit cu) const
{
	int n = 0;
	
	switch (cu)
	{
		case cuBYTE:
			n = 0;
			break;
		
		case cuKILOBYTE:
			n = 10;
			break;
		
		case cuMEGABYTE:
		    n = 20;
			break;
		
		default:
			n = 0;
			break;
	}
	
	return this->bytes_free >> n;
}


// -------------------------------------------------------------------------------------------------
// Returns the total capacity of the device.

int Device::getTotalCapacity (capacity_unit cu) const
{
	int n = 0;
	
	switch (cu)
	{
		case cuBYTE:
			n = 0;
			break;
		
		case cuKILOBYTE:
			n = 10;
			break;
		
		case cuMEGABYTE:
		    n = 20;
			break;
		
		default:
			n = 0;
			break;
	}
	
	return this->bytes_total >> n;
}


// -------------------------------------------------------------------------------------------------
// Calculates the free capacity again and updates member variable.

void Device::updateFreeCapacity ()
{
	TCHAR path [4] = _T("_:\\");
	path[0] = this->drive_letter;
		
	__int64 FreeBytesAvailable, TotalNumberOfBytes, TotalNumberOfFreeBytes;
	GetDiskFreeSpaceEx(path, (PULARGE_INTEGER)&FreeBytesAvailable, (PULARGE_INTEGER)&TotalNumberOfBytes, (PULARGE_INTEGER)&TotalNumberOfFreeBytes);
	
	this->bytes_free = TotalNumberOfFreeBytes;
}


// -------------------------------------------------------------------------------------------------
// Returns the mountpoint-letter based on an index to the list of currently available devices.

TCHAR Device::getDriveletter () const
{
	return this->drive_letter;
}


// -------------------------------------------------------------------------------------------------
// Returns the member volume_name (The name as it appears in the file explorer) as a std::wstring.

wstring Device::getVolumeName () const
{
	return wstring(this->volume_name);
}


// -------------------------------------------------------------------------------------------------
// Fills the member "description" with data.
//
// Looks in the registry for the entry of this device's ID and tries to get the value of either the
// key "FriendlyName" or "DeviceDesc".

void Device::retrieveDescription ()
{
	if (this->device_id && this->device_id[0])
	{
		HKEY Key;
		TCHAR KeyName[1024];
		
		wcscpy(KeyName, _T("SYSTEM\\CurrentControlSet\\Enum\\"));
		wcsncat(KeyName, this->device_id, sizeof KeyName - 1);
		
		KeyName[sizeof KeyName - 1] = _T('\0');
		LONG L = RegOpenKeyEx(HKEY_LOCAL_MACHINE, KeyName, 0, KEY_QUERY_VALUE, &Key);

		if (L == ERROR_SUCCESS)
		{
			DWORD Type;
			BYTE  Value[1024];
		
			DWORD ValueSize = sizeof Value;
			L = RegQueryValueEx(Key, _T("FriendlyName"), 0, &Type, (BYTE *)Value, &ValueSize);

			if (L != ERROR_SUCCESS)
				L = RegQueryValueEx(Key, _T("DeviceDesc"), 0, &Type, (BYTE *)Value, &ValueSize);
	
			RegCloseKey(Key);

			if ((L == ERROR_SUCCESS) && (Type == REG_SZ))
				this->description = (PWCHAR)Value;
		}
	}
}


// -------------------------------------------------------------------------------------------------
// Get device number for volume.

long Device::getDeviceNumber ()
{
	device_number = -1;

	std::wstring volume (device_path); // Something like "\\?\Volume{ce3492bf-d71a-11e0-b25e-4061869315ac}\".
	volume = volume.substr (0, volume.length()-1); // Remove trailing backslash (CreateFile doesn't like it).

	HANDLE volume_handle = CreateFile(volume.c_str(), 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	if (volume_handle == INVALID_HANDLE_VALUE)
		return -1;

	STORAGE_DEVICE_NUMBER sdn;
	DWORD BytesReturned = 0;

	long r = DeviceIoControl(volume_handle, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &sdn, sizeof(sdn), &BytesReturned, NULL);

	if (r)
		device_number = sdn.DeviceNumber;
	
	CloseHandle(volume_handle);

	if (device_number == -1)
		return -1;
	else
		return device_number;
}

// -------------------------------------------------------------------------------------------------
// Get correct device instance by matching device number.
// (Adjusted for different behaviour since Windows Vista.)
// http://www.codeproject.com/Articles/13839/How-to-Prepare-a-USB-Drive-for-Safe-Removal

DEVINST Device::getDeviceInstanceByDeviceNumber (long DeviceNumber)
{
	GUID  guid;
	BYTE  buffer [1024];
	DWORD required_size;

	SP_DEVICE_INTERFACE_DATA         devInterfaceData;
	SP_DEVINFO_DATA                  devInfoData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA pDevDetail;
	
	guid = GUID_DEVINTERFACE_DISK;
	
	HDEVINFO infoset = SetupDiGetClassDevs(&guid, NULL, NULL, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);

	if (infoset != INVALID_HANDLE_VALUE)
	{
		for (int index = 0; /* */ ; index++)
		{          
			ZeroMemory(&devInterfaceData, sizeof(devInterfaceData));
			devInterfaceData.cbSize = sizeof(devInterfaceData);

			if (!SetupDiEnumDeviceInterfaces(infoset, NULL, &guid, index, &devInterfaceData))
				break;

			ZeroMemory(&devInfoData, sizeof(devInfoData));
			devInfoData.cbSize = sizeof(devInfoData);

			pDevDetail  = (PSP_DEVICE_INTERFACE_DETAIL_DATA)buffer;
			pDevDetail->cbSize  = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			long r = SetupDiGetDeviceInterfaceDetail(infoset, &devInterfaceData,
			                                         pDevDetail,                 // SP_DEVICE_INTERFACE_DETAIL_DATA
			                                         1024, &required_size,       // Buffer size
			                                         &devInfoData);              // SP_DEVINFO_DATA

			if (r)
			{
				HANDLE drive = CreateFile(pDevDetail->DevicePath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

				if (drive != INVALID_HANDLE_VALUE)
				{
					STORAGE_DEVICE_NUMBER sdn;
					DWORD BytesReturned = 0;

					r = DeviceIoControl(drive, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &sdn, sizeof(sdn), &BytesReturned, NULL);

					if (r)
					{
						if (DeviceNumber == (long)sdn.DeviceNumber)
						{
							CloseHandle(drive);
							SetupDiDestroyDeviceInfoList(infoset);
							//this->devinst = devInfoData.DevInst; // Do this? get & set!
							return devInfoData.DevInst;
						}
					}

					CloseHandle(drive);
				}
			}
		}
	}

	SetupDiDestroyDeviceInfoList(infoset);
	return 0;	
}


// -------------------------------------------------------------------------------------------------
// Check and forward to a (recursive) method.

void Device::enumerateDevices ()
{
	DEVINST d = 0;
	
	if (CM_Locate_DevNode(&d, 0, CM_LOCATE_DEVNODE_NORMAL) == CR_SUCCESS)
		enumerateDevices_(d);
}

// -------------------------------------------------------------------------------------------------
// Goes through 'all' nodes and calls - if appropriate - subroutines to get more information about the device.
//
// Here we get the Device ID that is needed for the other routines.
//
// Note the appended "_": This routine is only meant for internal use! DO NOT CALL IT YOURSELF!
//
// [FIXME] Rename?

void Device::enumerateDevices_ (DEVINST parent)
{
	DEVINST this_di;
	
	if (CM_Get_Child(&this_di, parent, 0) != CR_NO_SUCH_DEVNODE)
	{
		ULONG dev_id_len;
		CM_Get_Device_ID_Size(&dev_id_len, this_di, 0);
		
		PWCHAR DeviceID = new TCHAR [dev_id_len + 1];
		CM_Get_Device_ID(this_di, DeviceID, dev_id_len+1, 0);
		
		ULONG status; // DN_...
		ULONG status_problem_number = 0; // Used only if DN_HAS_PROBLEM is set in 'status'.
		CM_Get_DevNode_Status(&status, &status_problem_number, this_di, 0);
		
		if (status & DN_REMOVABLE)
		{
			retrieveDescription();
		}
		
		delete DeviceID;
		
		enumerateDevices_(this_di);
	}
	
	DEVINST sibling;
	
	if (CM_Get_Sibling(&sibling, parent, 0) != CR_NO_SUCH_DEVNODE)
	{
		ULONG dev_id_len;
		CM_Get_Device_ID_Size(&dev_id_len, sibling, 0);
		
		PWCHAR DeviceID = new TCHAR [dev_id_len + 1];
		CM_Get_Device_ID(sibling, DeviceID, dev_id_len+1, 0);
		
		ULONG status; // DN_...
		ULONG status_problem_number; // Used only if DN_HAS_PROBLEM is set in 'status'.
		CM_Get_DevNode_Status(&status, &status_problem_number, sibling, 0);
		
		if (status & DN_REMOVABLE)
		{
			retrieveDescription();
		}

		delete DeviceID;

		enumerateDevices_(sibling);
	}
}


// -------------------------------------------------------------------------------------------------

void Device::eject ()
{
	CONFIGRET     r;
	PNP_VETO_TYPE VetoType;
	TCHAR         VetoName [MAX_PATH];
	DEVINST       devInstParent = 0;

	this->devinst = getDeviceInstanceByDeviceNumber(getDeviceNumber());
	CM_Get_Parent(&devInstParent, this->devinst, 0);

	// Occasionally, the first attempt doesn't work; so we wait and try again...
	for (int n = 1; n <= 3; n++)
	{
		r = CM_Request_Device_Eject(devInstParent, &VetoType, VetoName, MAX_PATH, 0);

		if (r == CR_SUCCESS)
			break;

		Sleep(500);
	}

	switch (r)
	{
		case CR_SUCCESS:
			//logfile << "CR_SUCCESS" << std::endl;
			break;

		case CR_INVALID_DEVNODE:
			//logfile << "CR_INVALID_DEVNODE" << std::endl;
			break;

		case CR_REMOVE_VETOED:
		{
			//logfile << "CR_REMOVE_VETOED" << std::endl;
		
			switch (VetoType)
			{
				case PNP_VetoTypeUnknown:   // The specified operation was rejected for an unknown reason.
				//logfile << "PNP_VetoTypeUnknown" << std::endl;
				break;

				case PNP_VetoLegacyDevice: // The device does not support the specified PnP operation.
				//logfile << "PNP_VetoLegacyDevice" << std::endl;
				break;

				case PNP_VetoPendingClose: // The specified operation cannot be completed because of a pending close operation.
				//logfile << "PNP_VetoPendingClose" << std::endl;
				break;

				case PNP_VetoWindowsApp: // A Microsoft Win32 application vetoed the specified operation.
				//logfile << "PNP_VetoWindowsApp" << std::endl;
				break;

				case PNP_VetoWindowsService: // A Win32 service vetoed the specified operation.
				//logfile << "PNP_VetoWindowsService" << std::endl;
				break;

				case PNP_VetoOutstandingOpen: // The requested operation was rejected because of outstanding open handles.
				//logfile << "PNP_VetoOutstandingOpen" << std::endl;
				break;

				case PNP_VetoDevice: // The device supports the specified operation, but the device rejected the operation.
				//logfile << "PNP_VetoDevice" << std::endl;
				break;

				case PNP_VetoDriver: // The driver supports the specified operation, but the driver rejected the operation.
				//logfile << "PNP_VetoDriver" << std::endl;
				break;

				case PNP_VetoIllegalDeviceRequest: // The device does not support the specified operation.
				//logfile << "PNP_VetoIllegalDeviceRequest" << std::endl;
				break;

				case PNP_VetoInsufficientPower: // There is insufficient power to perform the requested operation.
				//logfile << "PNP_VetoInsufficientPower" << std::endl;
				break;

				case PNP_VetoNonDisableable: // The device cannot be disabled.
				//logfile << "PNP_VetoNonDisableable" << std::endl;
				break;

				case PNP_VetoLegacyDriver: // The driver does not support the specified PnP operation.
				//logfile << "PNP_VetoLegacyDriver" << std::endl;
				break;

				default:
				break;
			}
			break;
		}

		default:
			break;
	}

	//logfile << VetoName << std::endl;
}
