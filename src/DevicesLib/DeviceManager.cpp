// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>

#include <windows.h>
#include <shellapi.h>
#include <tchar.h>

#include "DeviceManager.h"

// Static data members must be explicitly defined in one compilation unit (see "C++ FAQ Lite", 10.10 and 10.11).
bool DeviceManager::eject_when_done = false;
bool DeviceManager::clear_before_copying = false;
bool DeviceManager::clear_before_copying_all = true;
bool DeviceManager::clear_before_copying_filetypes = false;
int  DeviceManager::max_percent = 100; // Use max. <limit> % of available space on the device.
bool DeviceManager::use_directory = false;
std::wstring              DeviceManager::target_directory;
std::vector<std::wstring> DeviceManager::filetypes_to_delete;
unsigned int              DeviceManager::device_id;
    
// std::ofstream logfile("log.txt", std::ios_base::trunc);


// Utility routine, Win32-specific, for device handling (number to map drive letter).
//
char FirstDriveFromMask (ULONG unitmask)
{
	char i;
	for (i = 0; i < 26; ++i)
	{
		if (unitmask & 0x1)
			break;
		unitmask = unitmask >> 1;
	}
	return (i + 'A');
}


unsigned int DeviceManager::createID ()
{
	//EnterCriticalSection(&app.cs_dev_id);
	unsigned int r = device_id++;
	//LeaveCriticalSection(&app.cs_dev_id);
	return r;
}


int DeviceManager::getDeviceCount () const
{
	return device_list.size();
}


// Forwarding calls to the selected device.
//
int DeviceManager::getFreeCapacity (int index, capacity_unit cu) const
{
	return device_list.at(index)->getFreeCapacity(cu);
}


int DeviceManager::getTotalCapacity (int index, capacity_unit cu) const
{
	return device_list.at(index)->getTotalCapacity(cu);
}


void DeviceManager::updateFreeCapacity (int index)
{
	device_list.at(index)->updateFreeCapacity();
}


// Builds the internal list of currently available removable devices.
//
void DeviceManager::scanForDevices ()
{
	DWORD drive_bitmap = GetLogicalDrives();
	
	for (CHAR i = 0; i < 26; ++i)
	{
		if (drive_bitmap & 0x1)
		{	
			CHAR d = i + 'A';
			addDevice(d);
		}
		drive_bitmap = drive_bitmap >> 1;
	}
}


// Adds a device to the internal list.
//
bool DeviceManager::addDevice (CHAR driveletter)
{
	TCHAR path [4] = _T("_:\\");
	path[0] = driveletter;
	
	UINT dt = GetDriveType(path);

	if (dt == DRIVE_REMOVABLE)
	{
		Device* dev = new Device;
		
		if (dev)
		{
			dev->ID = createID();
			
			dev->drive_letter = path[0];
		
			__int64 FreeBytesAvailable, TotalNumberOfBytes, TotalNumberOfFreeBytes;
			GetDiskFreeSpaceEx(path, (PULARGE_INTEGER)&FreeBytesAvailable, (PULARGE_INTEGER)&TotalNumberOfBytes, (PULARGE_INTEGER)&TotalNumberOfFreeBytes);
			dev->bytes_total = TotalNumberOfBytes;
			dev->bytes_free  = TotalNumberOfFreeBytes;

			TCHAR FileSysNameBuf[MAX_PATH];
			DWORD dwSysFlags;
			
			GetVolumeInformation(path, dev->volume_name, 128, NULL, NULL, &dwSysFlags, FileSysNameBuf, MAX_PATH);
			GetVolumeNameForVolumeMountPoint(path, (LPWSTR) dev->device_path, sizeof dev->device_path);
			
			dev->enumerateDevices(); // Getting DeviceID and mapping the drive letter to the correctdevice (info needed later for 'eject device').
			
			device_list.push_back(dev);
			return true;
		}
	}
	return false;
}


// Removes a device from the list.
//
void DeviceManager::removeDevice (TCHAR driveletter)
{
	for (std::vector<Device*>::iterator pos = device_list.begin(); pos != device_list.end(); ++pos)
	{
		if ((*pos)->drive_letter == driveletter)
		{
			delete *pos;
			device_list.erase(pos);
			return;
		}
	}
}


// Erases the whole list and frees the memory.
//
void DeviceManager::clearList ()
{
	for (std::vector<Device*>::iterator pos = device_list.begin(); pos != device_list.end(); ++pos)
	{
		delete *pos;
		device_list.erase(pos++);
	}
}


// Empties a device (only files of type *.ext).
//
void DeviceManager::emptyDevice (int index, std::vector<std::wstring> list)
{
	device_list.at(index)->empty(list);
	updateFreeCapacity(index);
}


void DeviceManager::emptyDevice (int index)
{
	std::vector<std::wstring> list; // Dummy since an empty value is needed.
	device_list.at(index)->empty(list);
	updateFreeCapacity(index);
}


// Tries to eject a device.
//
void DeviceManager::ejectDevice (int index)
{
	device_list.at(index)->eject();
}


// Returns the mountpoint-letter based on an index to the list of currently available devices.
//
TCHAR DeviceManager::getDriveletter (int index) const
{
	return device_list.at(index)->getDriveletter();
}


// Returns the volume name (rhe name as it appears in the file explorer) as a std::wstring.
//
std::wstring DeviceManager::getVolumeName (int index) const
{
	return device_list.at(index)->getVolumeName();
}


bool DeviceManager::onDeviceArrival (PDEV_BROADCAST_HDR lpdb)
{
	if (lpdb->dbch_devicetype == DBT_DEVTYP_VOLUME)
	{
		PDEV_BROADCAST_VOLUME lpdbv = (PDEV_BROADCAST_VOLUME)lpdb;
		addDevice(FirstDriveFromMask(lpdbv->dbcv_unitmask));		
		return true;
	}
	else
	{
		return false;
	}
}


bool DeviceManager::onDeviceRemoval (PDEV_BROADCAST_HDR lpdb)
{
	if (lpdb->dbch_devicetype == DBT_DEVTYP_VOLUME)
	{
		PDEV_BROADCAST_VOLUME lpdbv = (PDEV_BROADCAST_VOLUME)lpdb;
		removeDevice(FirstDriveFromMask(lpdbv->dbcv_unitmask));
		return true;
	}
	else
	{
		return false;
	}
}


std::wstring DeviceManager::getTargetDirectory ()
{
	return target_directory;
}


void DeviceManager::setTargetDirectory (std::wstring x)
{
	target_directory = x;
}


bool DeviceManager::isTargetDirectoryEmpty ()
{
	return target_directory.empty();
}


std::vector<std::wstring> DeviceManager::getFiletypesToDelete ()
{
	return filetypes_to_delete;
}


std::wstring DeviceManager::getFiletypesToDeleteAsText ()
{
	std::wstring output = joinValue(filetypes_to_delete);
	return output;
}


void DeviceManager::setFiletypesToDelete (std::wstring x)
{
	filetypes_to_delete = splitValue(x); // TODO?: More delimiters (,;)?
}


/*
	Splits a given string s into chunks.

	A value-string can be split when each component is separated either by a comma and/or space.
	A component itself can contain space, if it is enclosed in double-quotation-marks.
	This one will produce five chunks: value-1, "string with space" and more, values_2
	
	"s" represents the 'value' part of the key/value pair.
	
	Returns a vector of strings, each element of that vector represents a chunk of the splitted input value.
*/
std::vector<std::wstring> DeviceManager::splitValue (std::wstring s)
{
	std::wstring::iterator end = s.end();
	
	std::vector<std::wstring> output;
	std::wstring temp;
	
	for (std::wstring::iterator pos = s.begin(); pos != end; ++pos)
	{
		if (isspace(*pos) || *pos == ',')
		{
			continue;
		}
		else
		{
			if (*pos == '"' && (pos+1) != end)
			{
				++pos;
				
				while (*pos != '"' && (pos+1) != end)
				{
					temp += *pos;
					++pos;
				}
				
				output.push_back(temp);
				temp.clear();
			}		
			else
			{
				while ((*pos != ',' && !isspace(*pos)) && (pos+1) != end)
				{
					temp += *pos;
					++pos;
				}
				
				if (*pos != ',' && !isspace(*pos))  // The previous loop exits one character before the end.
					temp += *pos;
				
				output.push_back(temp);
				temp.clear();
			}
		}
	}

	return output;
}


// Joins a vector of strings into one (comma-separated) string.

std::wstring DeviceManager::joinValue (std::vector<std::wstring> input)
{
	std::wstring s;
	
	if (input.empty())
	{
		return s;
	}
	else
	{
		for (std::vector<std::wstring>::iterator pos = input.begin(); pos != input.end(); ++pos)
		{
			if (pos == (input.end()-1) || input.size() == 1)
				s.append(*pos);
			else
				s.append(*pos + std::wstring(1, ','));
		}	
		return s;
	}
}
