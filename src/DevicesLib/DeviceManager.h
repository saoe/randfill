// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include <vector>

#include <windows.h>
#include <dbt.h>

#include "Device.h"

class DeviceManager
{
	public:
		void scanForDevices ();
		
		int  getFreeCapacity (int index, capacity_unit cu) const;
		int  getTotalCapacity (int index, capacity_unit cu) const;
		void updateFreeCapacity (int index);		
		
		int          getDeviceCount () const;
		TCHAR        getDriveletter (int index) const;
		std::wstring getVolumeName (int index) const;
		
		void emptyDevice (int index);
		void emptyDevice (int index, std::vector<std::wstring> list);
		
		void ejectDevice (int index);
		
		bool onDeviceArrival (PDEV_BROADCAST_HDR lpdb);
		bool onDeviceRemoval (PDEV_BROADCAST_HDR lpdb);

		static std::wstring getTargetDirectory ();
		static void         setTargetDirectory (std::wstring x);
		static bool         isTargetDirectoryEmpty ();

		static std::vector<std::wstring> getFiletypesToDelete ();
		static std::wstring              getFiletypesToDeleteAsText ();
		static void                      setFiletypesToDelete (std::wstring x);
		
		static bool eject_when_done;
		static bool clear_before_copying;
		static bool clear_before_copying_all;
		static bool clear_before_copying_filetypes;
		static int  max_percent; // Use max. <limit> % of available space on the device.
		static bool use_directory;

	private:
		bool addDevice (CHAR driveletter);
		void removeDevice (TCHAR driveletter);
		
		unsigned int createID ();
		
		void clearList ();

		static std::vector<std::wstring> splitValue (std::wstring s);
		static std::wstring joinValue (std::vector<std::wstring> input);
		
		std::vector<Device*> device_list;  // Pointer! => cpyctor/assignop!
		static unsigned int  device_id;
		
		static std::wstring              target_directory;
		static std::vector<std::wstring> filetypes_to_delete;
};

#endif
