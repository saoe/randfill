// ☺
// Copyright © 2005-2015 Sascha Offe <so@saoe.net>.
// Published under the terms of the MIT license (see accompanied file "License.txt" or visit <http://opensource.org/licenses/MIT>).

#ifndef DEVICE_H
#define DEVICE_H

#include <vector>

typedef DWORD DEVINST, *PDEVINST; // Officially defined in cfgmgr32.h

enum capacity_unit
{
	cuBYTE,
	cuKILOBYTE,
	cuMEGABYTE
};

class Device
{
	friend class DeviceManager; // Hmmm...?
	
	public:
		int          getFreeCapacity (capacity_unit cu) const;
		int          getTotalCapacity (capacity_unit cu) const;
		TCHAR        getDriveletter () const;
		std::wstring getVolumeName () const;

		void updateFreeCapacity ();
		void enumerateDevices ();
		void empty (std::vector<std::wstring> input) const;
	
	private:
		unsigned int ID;           // Object's internal ID.
		
		TCHAR   drive_letter;       // For example 'F' for "F:\".
		TCHAR   device_id   [1024]; // Device ID, supplied by Windows. Example: USB\VID_03EB&PID_2002\4710765066451
		long    device_number;      // Device number, used to map a storage volume to the matching disk (needed since Windows Vista).
		TCHAR   volume_name [1024]; // The name as it appears in the file explorer.
		TCHAR   device_path [1024]; // Device path, supplied by Windows, with {GUID}. Example: \\?\Volume{150c93ad-5056-11da-8321-000ea674c509}\ (with trailing backslash! [Required by some functions, so leave it!])
		DEVINST devinst;            // Opaque handle to the device instance (also known as a handle to the devnode).
		
		std::wstring description;   // Either the device's "FriendlyName" or "DeviceDesc" from the Windows registry.
				
		long    getDeviceNumber ();
		DEVINST getDeviceInstanceByDeviceNumber (long DeviceNumber);
		void retrieveDescription ();
		void eject ();
		void enumerateDevices_ (DEVINST parent); // DO NOT CALL!
		
		bool eject_when_done;
		unsigned long bytes_total;
		unsigned long bytes_free;
};

#endif
