<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Application/MainWindow.cpp" line="855"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="856"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="857"/>
        <source>EN (English)</source>
        <translation>EN (Englisch)</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="858"/>
        <source>DE (German)</source>
        <translation>DE (Deutsch)</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="860"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="801"/>
        <location filename="../Application/MainWindow.cpp" line="861"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="413"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="413"/>
        <source>No files found in %1!
Try to include the sub-directories or select another directory.</source>
        <translation>Keine Dateien in %1 gefunden!
Mit Unterverzeichnissen probieren oder ein anderes Verzeichnis auswählen.</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="731"/>
        <source>%1 MB available (of %2 MB in total).</source>
        <translation>%1 MB verfügbar (von %2 MB insgesamt).</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="862"/>
        <source>Open help in Browser</source>
        <translation>Öffne Hilfe im Browser</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="865"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="866"/>
        <source>Change...</source>
        <translation>Ändern...</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="867"/>
        <source>Include sub-directories</source>
        <translation>Mit Unterverzeichnissen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="868"/>
        <source>Add path</source>
        <translation>Pfad hinzufügen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="869"/>
        <source>Directories:</source>
        <translation>Verzeichnisse:</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="870"/>
        <source>Remove path</source>
        <translation>Pfad entfernen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="871"/>
        <source>Clear</source>
        <translation>Aufräumen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="872"/>
        <location filename="../Application/MainWindow.cpp" line="878"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="873"/>
        <source>Filter...</source>
        <translation>Filter...</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="874"/>
        <source>Generate Playlist File</source>
        <translation>Playlist-Datei erzeugen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="877"/>
        <source>Target</source>
        <translation>Ziel</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="880"/>
        <source>Use max. % of free capacity:</source>
        <translation>Max. % der freien Kapazität nutzen:</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="881"/>
        <source>Copy files into this directory:</source>
        <translation>Dateien in dieses Verzeichnis kopieren:</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="882"/>
        <source>Empty device before copying</source>
        <translation>Gerät vor dem Kopiervorgang leeren</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="883"/>
        <source>Delete everything under this path:</source>
        <translation>Alles unterhalb dieses Pfades löschen:</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="884"/>
        <source>Deletes all files on the selected target drive.
If a target directory is given, this will act only within that directory (if it already exists).
CAUTION: This will delete the content without asking for confirmation and without putting the files in the recycle bin!</source>
        <translation>Alle Dateien auf dem ausgewählten Ziellaufwerk löschen.
Falls ein Zielverzeichnis angebeben wurde, wird nur darin gelöscht.
VORSICHT: Der Inhalt wird ohne Nachfrage gelöscht (und ohne Papierkorb)!</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="885"/>
        <source>Delete only these filetypes:</source>
        <translation>Nur diese Dateitypen löschen:</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="886"/>
        <source>Deletes only files of given type(s).
Enter the plain extension (example: TXT for *.TXT), seperate multiple values with a comma (example: TXT,MP3,MKV).
Acts non-recursively on the directory.
CAUTION: This will delete the content without asking for confirmation and without putting the files in the recycle bin!</source>
        <translation>Nur diese Dateitypen löschen.
Die einfache Erweiterung angeben (Beispiel: TXT für *.TXT), mehrere Werte mit Komma trennen (Beispiel: TXT,MP3,MKV).
Bearbeitet das Verzeichnis nicht-rekursiv.
VORSICHT: Der Inhalt wird ohne Nachfrage gelöscht (und ohne Papierkorb)!</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="887"/>
        <source>Eject device when done</source>
        <translation>Gerät auswerfen (abmelden) sobald fertig</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="890"/>
        <source>Begin</source>
        <translation>Beginnen</translation>
    </message>
    <message>
        <location filename="../Application/MainWindow.cpp" line="891"/>
        <source>Select source directory</source>
        <translation>Quellverzichnis auswählen</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../Application/ProgressDialog.cpp" line="126"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message numerus="yes">
        <location filename="../Application/ProgressDialog.cpp" line="77"/>
        <source>%n file(s) copied...</source>
        <translation>
            <numerusform>%n Datei kopiert...</numerusform>
            <numerusform>%n Dateien kopiert...</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../Application/ProgressDialog.cpp" line="85"/>
        <source>Copying aborted! Error code: 0x%1</source>
        <translation>Kopiervorgang abgebrochen! Fehlercode: 0x%1</translation>
    </message>
    <message numerus="yes">
        <location filename="../Application/ProgressDialog.cpp" line="93"/>
        <source>Done! %n file(s) copied.</source>
        <translation>
            <numerusform>Fertig! %n Datei kopiert.</numerusform>
            <numerusform>Fertig! %n Dateien kopiert.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../Application/ProgressDialog.cpp" line="117"/>
        <source>Copying aborted by user!</source>
        <translation>Kopiervorgang durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location filename="../Application/ProgressDialog.cpp" line="125"/>
        <source>Progress</source>
        <translation>Fortschritt</translation>
    </message>
    <message>
        <location filename="../Application/ProgressDialog.cpp" line="127"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
</TS>
