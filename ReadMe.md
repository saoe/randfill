# RandFill

An utility program for copying files in a shuffled order onto a removable USB mass storage device;
written for the purpose of filling a portable MP3 player with random files from a local music archive.

Runs on Microsoft Windows 10, 8, Vista and 7; Windows XP is no longer supported.


## Download (pre-built binaries/installer executable)

You can get the installer EXE from either the [Homepage](http://www.saoe.net/software/randfill) or
the [Project page](https://bitbucket.org/saoe/randfill/) on _BitBucket.org_ (see "Downloads" on the left pane).


## How to build from source

### Requirements, preferred tools and dependencies

- [Source code](https://bitbucket.org/saoe/randfill) from the Git repository on BitBucket.

- [CMake](http://www.cmake.org): Build tool.

- [Microsoft Visual Studio](https://visualstudio.microsoft.com/): Compiler & IDE  
  For Version 2017/15.x and newer: Also install the Windows 10 SDK (_"Desktop development with C++"_ workload).
  
- [WPDLib](https://bitbucket.org/saoe/wpdlib): Optional to get, build and install it by yourself.  
  If not, it will be fetched and incorporated in RandFill's build process automatically anyway.

- [Qt](http://qt-project.org/) framework: GUI & more  
  (See notes on how I built it: http://www.saoe.net/blog/tag/qt/)

- [Nullsoft Scriptable Install System (NSIS)](http://nsis.sourceforge.net>): Optional; to build the installer.


### 1. Run CMake to configure the build

Open a command line prompt in the top-level directory of your project:

	C:\>cd path\to\the\project

Then run cmake.exe there with arguments that fit to the setup of your specific environment.

Example (with locally installed Qt and WPDLib somewhere):

	cmake.exe -D CMAKE_PREFIX_PATH=C:\devel\ext\Qt\5.12.3\_64DLL\lib\cmake\Qt5;C:\devel\int\WPDLib\cmake -S src -B _build -G "Visual Studio 16 2019" -A x64

Example (with locally installed Qt somewhere, but no pre-installed WPDLib. WPDLib will be fetched
from its Git repository and incorporated into the build step):

	cmake.exe -D CMAKE_PREFIX_PATH=C:\devel\ext\Qt\5.12.3\_64DLL\lib\cmake\Qt5 -S src -B _build -G "Visual Studio 16 2019" -A x64

The parameters are:

 Option         | Description
----------------|-----------------------------------------------------------------------------------
 -D <X=Y>       | Definitions (must come first!).
                | Set `CMAKE_PREFIX_PATH` to the path where the CMake Config files of the external dependecies are;
				| for example for _Qt_ (i.e. Qt5Config.cmake, etc.) and _WPDLib_ (but if WPDLib is not found, it will be fetched from the net; it's small enough).
 -S <Path>      | Path to the source code files and the top-level CMakeLists.txt. (available since CMake 3.13)
 -B <Path>      | Path to the out-of-source build directory. (available since CMake 3.13)
 -G <Generator> | Optional: Name of the generator (e.g. "Visual Studio 16 2019").
                | If you omit this, CMake will use the default generator.
 -A <Platform>  | Name of the target architecture/platform (if supported by the generator, e.g. 'Win32' or 'x64' for MSVC).


### 2. Build...

#### 2.1 ... with CMake on the command line

CMake invokes the native tools, like Visual C++, for the actual building.  

Example:
    
	cmake.exe --build _build --target ALL_BUILD --config Release

#### 2.2 ... with the Visual Studio IDE

Use the generated `*.sln` file in the build directory to open the solution with the IDE and build from there.

____________________________________________________________________________________________________

## Copyright & License

Copyright © 2005-2019 Sascha Offe (so@saoe.net).  
Published under the terms of the [MIT license](http://opensource.org/licenses/MIT>) (see accompanying file [License.txt](License.txt) for details).
